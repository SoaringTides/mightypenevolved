/*
 *
 * authors: Benjamin Shu, Peter Ly, Stefania Zimmerman
 *
 */

/* Initialize variables for storing Comic data. */
var userID, user, userSeries, series, comicsList, comic, title, comicsDisplay;

/* Initialize variables for HTML control elements. */
var seriesList, titleInput, pageInput, tagsList,
	editButton, addButton, deleteButton, changeButton;
	
/* Initialize Boolean lock for database requests. */
var loading = false, loadCount = 0;

var currentUser;
var currentUserId;
var userResultsHolder;
var currentComicHolder;

function toggleOverlayStartPage(){
    //window.alert("Test");
    var div = document.getElementById('start-page-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";

      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}


function toggleOverlay(){
    //window.alert("Test");
    var div = document.getElementById('edit-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";

      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}

function toggleOverlayAdd(){
	//window.alert("Test");
	var div = document.getElementById('add-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";

      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}

function toggleOverlayDelete(){
	//window.alert("Test");
	var div = document.getElementById('delete-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";

      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}


function loadSeries(callback) {
	if (!loading) {
		/* Request every Series associated with current user. */
        userSeries = new Array();
        var i = 0;
        loading = true;
        $.get('/byUser/series/' + userID, function(data) {
            userSeries = data;
            loading = false;
            callback();
        }).fail(function() {
            console.log("Could not load user's Series entries.");
            loading = false;
        });
	}
}
function updateSeries() {
	seriesList.innerHTML = "";
	seriesStr = "";
	var i = 0;
	for (i = 0; i < userSeries.length; i++) {
		seriesStr += "<option value=\"" + userSeries[i].title + "\">" + userSeries[i].title + "</option>";
	}
	seriesList.innerHTML = seriesStr;
}

function loadComics(callback) {
	if (!loading) {
        /* Request all Comics associated with current user. */
		comicsList = new Array();
		var i = 0, k = 0;
		loadCount = userSeries.length;
		for (i = 0; i < userSeries.length; i++) {
			 loading = true;
			 $.get('/Series/series/' + userSeries[i].seriesId + '/comics', function(data) {
				 for (k = 0; k < data.length; k++) {
					 comicsList.push(data[k]);
				 }
				 loading = false;
				 loadCount--;
				 if (loadCount == 0) { loading = false; callback(); }
			 }).fail(function() {
				 console.log("Could not load comic " + i + ".");
				 loadCount--;
				 if (loadCount == 0) { loading = false; callback(); }
			 });
		 }
	}
}
function updateComics() {
	comicsDisplay.innerHTML = "";
	comicsStr = "";
	var i = 0;
	for (i = 0; i < comicsList.length; i++) {
		comicsStr += "<div class=\"returned-comic\">" + comicsList[i].title + "</div>";
	}
	comicsDisplay = comicsStr;
}

function addComic() {
	
}

function editComic() {
	
}
////////////////////////////////////////////////////////////////////
function searchByUserName(callback) {
	/* Only attempt search if not already waiting for data. */
	if (!loading) {
		console.log("Searching to get comics of a user...");
		/* Query database using user-entered title. */
		loading = true;
		// $.get('/Series/title/' + title, function(data) {
		//$.get('/byUser/userName/' + title, function(data) {
		console.log(currentUser);
		$.get('/byUser/userName/' + currentUser, function(data) {
			userResultsHolder = data;
			console.log(userResultsHolder);
			console.log(userResultsHolder.userID);
			loading = false;
			callback();
		}).fail(function() {
			console.log("Could not get user.");
			loading = false;
		});
	}
}


function getActualComics(callback) {
	/* Only attempt search if not already waiting for data. */
	if (!loading) {
		console.log("Searching to get comics based on userid...");

		$.get('/Comics/comicUserId/' + userResultsHolder.userID, function(data) {
			results = data;
			loading = false;
			callback();
		}).fail(function() {
			console.log("Could not search by userid.");
			loading = false;
		});


	}
}

function search() {
	//event.preventDefault();
	/* Only attempt search if not already waiting for data. */
	if (!loading) {
		/* Load input from HTML page into variables. */
		usernameValue = currentUser;

		console.log("username: " + usernameValue);

		searchByUserName(function() {
			getActualComics(function() {
				displayResults();
			});
		});
	}
}

function displayResults() {
	var i = 0, resultsStr = "";
	console.log(results);
	console.log("THE ONLY RESULTS CALL WE SHOULD BE GETTING:");
	for (i = 0; i < results.length; i++) {
		resultsStr += "<div class=\"returned-comic-wrapper\"><div class=\"returned-comic\">Title:" + results[i].title
			+ "<br>Desc.:" + results[i].description + "</div></div>";
	}
	document.getElementById("results").innerHTML = resultsStr;
	/*
    document.getElementById("results").innerHTML = "<div class=\"returned-comic\">" + results.title + "<br>"
    + results.description;

     */
};


var descComicHolder;
function changeDescription() {
	newdesc = document.getElementById("newdesc");
	//comicname = document.getElementById("comicname");
	comicnewname = document.getElementById("comicnewname");
	if(currentComicHolder == null){
		document.getElementById("errorSaveMessage").innerHTML = "Please enter a comic name."
	}
	else{

		console.log("hi, inside else case of changedescription");
			$.get('/Comics/comicTitle/' + currentComicHolder.title, function(data) {
				descComicHolder = data;
				console.log(descComicHolder);
				console.log(descComicHolder.title);
				finishDescriptionUpdate(newdesc,comicnewname);
			}).fail(function() {
				console.log("Could not get comic.");
			});

	}
};



function deleteComic() {
	deletedname = document.getElementById("deletecomicname");
	//console.log(deletedname);


	if(deletedname.value == ""){
		document.getElementById("errorSaveMessage3").innerHTML = "Please enter a comic name."
	}
	else{
		$.post('/Comics/deleteComicByUserIdScan/' + deletedname.value, function() {
			console.log("Page has made a post-delete call!");
			//emptyPost();
			window.open ('manageComic','_self',false);
			//location.reload();
		}).fail(function() {
			console.log("Could not upload new Page.");
		});
	}


};


function finishDescriptionUpdate(newdesc,comicnewname) {
	newDescription = newdesc.value;
	newName = comicnewname.value;
	console.log("VALUES BEFORE");
	console.log(newDescription)
	console.log(newName)

	if(comicnewname.value == ""){
		newName = descComicHolder.title;
	}

	if(newdesc.value== ""){
		newDescription = descComicHolder.description;
	}

	console.log("VALUES AFTER");
	console.log(newDescription)
	console.log(newName)

	$.post('/updateComicDescriptionFlexible/' + currentComicHolder.title + '/' + newDescription + '/' + newName, function() {
		console.log("Page has made a post call!");
		//emptyPost();
		window.open ('manageComic','_self',false);
		//location.reload();
	}).fail(function() {
		console.log("Could not upload new Page.");
	});
};

////////////////////////////////////////////////////////////////////
window.onload = function() {
	/* Retrieve all HTML control elements. */
    seriesList      = document.getElementById("seriesList");
    titleInput      = document.getElementById("titleInput");
    pageInput       = document.getElementById("pageInput");
    tagsList        = document.getElementById("tagsList");
    editButton      = document.getElementById("editButton");
    addButton       = document.getElementById("addButton");
    deleteButton    = document.getElementById("deleteButton");
	changeButton    = document.getElementById("changeButton");

	currentUser = document.getElementById("username").innerHTML;
	
	/* Initialize control handlers.
    addButton.onclick		= addComic;
    deleteButton.onclick	= deleteComic;
    editButton.onclick		= editComic;
    */
	changeButton.onclick	= changeDescription;
	deleteButton.onclick	= deleteComic;

	search();
        //window.alert("test two");
        $Divs = $(".search-results");
        //console.log("Divs are"+$Divs.length);

    $Divs.click(function() {
        //window.alert("click recorded")
        //$Divs.removeClass("highlight");
        //$(this).addClass("highlight");

        if ($(event.target)[0] === $Divs[0]) { return; }

        var comicTitle = $(event.target).text();
        //window.alert(seriesTitle);
        var index = comicTitle.lastIndexOf("Desc.:");
		comicTitle = comicTitle.substring(6,index);
        //window.alert("Currently selected series is "+seriesTitle);
		document.getElementById("comicTitle").innerHTML = comicTitle;
		//if (tagOverlay.display != "none") {
			//tagsList.innerHTML = "";
			$.get("/Comics/comicTitle/" + comicTitle, function(data) {
				console.log("Got comic!");
				console.log(data);
				currentComicHolder = data;
			});
		//}
    });


};