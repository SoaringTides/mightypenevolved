/*
 *
 * authors: Benjamin Shu, Peter Ly, Stefania Zimmerman
 *
 */

/* Initialize variables for the HTML control elements. */
var titleInput, authorInput, tagInput, searchButton, resultsList;

/* Initialize variable for HTML search type dropdown. */
var searchType, resultsType;

/* Initialize variables for storing input and results. */
var title, author, tag, results;

/* Initialize Boolean locks for requesting/filtering data. */
var loading = false, filtering = false, loadCount = 0;

function searchByTitle(callback) {
	/* Only attempt search if not already waiting for data. */
	if (!loading) {
	    console.log("Searching by title...");
		/* Query database using user-entered title. */
		 loading = true;
		 if (resultsType == "Series") {
			// $.get('/Series/title/' + title, function(data) {
			//$.get('/byUser/userName/' + title, function(data) {
			 $.get('/SDC/SeriesPartialSearch/' + title, function(data) {
				resultsList = data;
				loading = false;
				callback();
			 }).fail(function() {
				console.log("Could not search for series by title.");
				loading = false;
			 });
		 } else if (resultsType == "Comic") {
		    $.get('/getComicByPublishedTitle/' + title, function(data) {
		 	    resultsList = data;
		 	    console.log(data);
		 	    loading = false;
		 	    callback();
		 	}).fail(function() {
		 	    console.log("Could not search for comic by title.");
		 	    loading = false;
		 	});
		 }
	}
}
function filterByTitle(callback) {
    if (!filtering) {
        filtering = true;
        console.log("Filtering by title...");
        /* Iterate through results list and remove entries that do not match title. */
        var i = 0, filtered = new Array();
        for (i = 0; i < results.length; i++) {
            if (resultsList[i].title && resultsList[i].title == title) {
                filtered.push(resultsList[i]);
            }
        }
        resultsList = filtered;
        filtered = false;
        callback();
    }
}

function searchByAuthorDeprecated(callback) {
	/* Only attempt search if not already waiting for data. */
	if (!loading) {
	    console.log("Searching by author..." + author);
		/* Query database using author's name. */
		loading = true;
		$.get('/SDC/SeriesPartialSearchAuthor/' + author, function(data) {
		    resultsList = data;
			loading = false;
			callback();
		}).fail(function(){
			alert("Could not search by author - 1.");
			loading = false;
		});
	}
}

var authorholder;
/*Part 1 of author search - get user NAME from user ID*/
function searchByAuthorConvertId(callback) {
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        console.log("Searching by author..." + author);
        /* Query database using author's name. */
        loading = true;
        $.get('/byUser/userName/' + author, function(data) {
            authorholder = data;
            //authorholder's responsibilty is to temporarily hold the
            //user data until the approrpiate operations are performed in it
            //in searchByAuthorLocateById
            loading = false;
            searchByAuthorLocateById(callback);
        }).fail(function(){
            alert("Could not search by author - 2.");
            loading = false;
        });
    }
}


/*Part 2 of author search - using the user NAME, find all comics, and then call the callback.*/
function searchByAuthorLocateById(callback) {
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        console.log("Searching by author..." + author);
        /* Query database using author's name. */
        loading = true;
        if (resultsType == "Series") {
            $.get('/SDC/SeriesPartialSearchAuthor/' + authorholder.userID, function(data) {
                resultsList = data;
                loading = false;
                callback();
            }).fail(function(){
                alert("Could not search by author - 3.");
                loading = false;
            });
        } else if (resultsType == "Comic") {
            $.get('/getComicByUserID/' + authorholder.userID, function(data) {
                resultsList = data;
                loading = false
                callback();
            }).fail(function() {
                alert("Could not search for comics by author.");
                loading = false;
            });
        }
    }
}


function filterByAuthorDeprecated(callback) {
    if (!filtering) {
        filtering = true;
        if (author != "") {
			console.log("Filtering by author...");
            /* Iterate through results list and remove entries not created by requested author. */
            var i = 0, filtered = new Array();
            for (i = 0; i < resultsList.length; i++) {
                console.log(resultsList[i].userId);
                //changed to allow partial matching
                if (resultsList.userId && ((String)(resultsList[i].userId)).includes(author)) {
                    filtered.push(resultsList[i]);
                }
            }
            resultsList = filtered;
        }
        filtering = false;
        callback();
    }
}


/*Part 1 of author filter - get user NAME from user ID*/
function filterByAuthorConvertId(callback) {
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        /* Query database using author's name. */
        loading = true;
        if (author != "") {
            console.log("Searching by author..." + author);
            $.get('/byUser/userName/' + author, function(data) {
                authorholder = data;
                console.log(data);
                //authorholder's responsibilty is to temporarily hold the
                //user data until the approrpiate operations are performed in it
                //in searchByAuthorLocateById
                loading = false;
                filterByAuthorLocateById(callback);
            }).fail(function(){
                alert("Could not search by author - 4.");
                loading = false;
            });
        } else{
            console.log("Not filtering by author - go to callback!");
            loading = false;
            callback();
        }
    }
}



/*Part 2 of author filter - using the user NAME, filter  all comics, and then call the callback.*/
function filterByAuthorLocateById(callback) {
    if (!filtering) {
        //console.log(typeof(authorholder.userID));
        filtering = true;
        if (author != "") {
            console.log("Filtering by author..." + authorholder.userID);
            /* Iterate through results list and remove entries not created by requested author. */
            var i = 0, filtered = new Array();
            for (i = 0; i < resultsList.length; i++) {
                console.log(resultsList[i].userId);
                //changed to allow partial matching
                //console.log("is boolean valid?");
                //console.log(((String)(resultsList[i].userId)) == (authorholder.userID));
                if (((String)(resultsList[i].userId)) == (authorholder.userID)) {
                    filtered.push(resultsList[i]);
                }
            }
            resultsList = filtered;
        }
        filtering = false;
        console.log("Part 2 done - next filter!");
        callback();
    }
}

function searchByTags(callback) {
	/* Only attempt search if not already waiting for data. */
	if (!loading) {
		if (resultsType == "Series") {
			/* Query database using individual tags. */
	        loading = true;
	        if (tag != "") {
		        console.log("Searching by tags...");
	            /* Perform a database query for every listed tag. */
	            $.get('/Series/tag/' + tag, function(data) {
	                resultsList = data;
	                loading = false;
	                callback();
	            }).fail(function(){
	                alert("Could not load results - please try again.");
	                loading = false; return;
	            });
	        }
		}
	}
}
function filterByTags(callback) {
    if (!filtering) {
        if (resultsType == "Series") {
	        filtering = true;
	        if (tag != "") {
		        console.log("Filtering by tags...");
	            /* Only accept results that have one or more of the specified tags. */
	            var i = 0, j = 0, filtered = new Array();
	            for (i = 0; i < resultsList.length; i++) {
	                if (resultsList[i].tag && resultsList[i].tag.indexOf(tag) > -1) {
	                    filtered.push(resultsList[i]);
	                }
	            }
	            resultsList = filtered;
	        }
        }
        filtering = false;
        console.log("Tags - callback!");
        callback();
    }
}

var authorList=[];
var authorDict={};
function search(event) {
    event.preventDefault();
	/* Only attempt search if not already waiting for data. */
	if (!loading) {
		/* Load input from HTML page into variables. */
		title = titleInput.value;
		author = authorInput.value;
		tag = tagInput.value;

		resultsType = searchType.value;

		console.log("Title: " + title);
		console.log("Author: " + author);
		console.log("Tag: " + tag);
		
		/* If input fields are blank, do nothing. */
		if (title != "") {
            //searchByTitle(displayResults);
			searchByTitle(function() {
                filterByAuthorConvertId(function() {
			        filterByTags(function() {
                        //testcallback();
                        constructAuthorList();
                        //console.log(authorList);
			            //displayResults();
			        });
			    });
			});
		} else if (author != "") {
		    console.log("gate 2");
            searchByAuthorConvertId(function() {
                    filterByTags(function() {
                        //testcallback();
                        constructAuthorList();
                        //console.log(authorList);
                        //displayResults();
                    });
            });
			//searchByAuthor(filterByTags(displayResults));
		} else {
			searchByTags(function() {
                //testcallback();
                constructAuthorList();
                //console.log(authorList);
                //displayResults();
            });
		}
	}
}

function testcallback() {
    console.log("BLACK PANTHER");
}

function constructAuthorList(){
    //authorList=[];
    console.log("authorlist function");
    //console.log(resultsList);
    //console.log(resultsList.length > 0);
    authorDict={};
    if (resultsList.length > 0) {
        for (i = 0; i < resultsList.length; i++) {
            //console.log(resultsList[i].userId);
            $.get('/byUser/userId/' + resultsList[i].userId, function(data) {
                authorholder = data;
                //authorholder's responsibilty is to temporarily hold the
                //user data until the approrpiate operations are performed in it
                //in searchByAuthorLocateById
                //console.log(authorholder);
                //console.log(authorholder.userName);
                loading = false;
                //authorNameList[i] = authorholder.userName;

                //authorList.splice(i, 0, authorholder.userName)
                authorDict[authorholder.userID] = authorholder.userName

                // resultsList[i].userId=authorholder.userName;
                //authorNameList.push(authorholder);
                //console.log(authorList);
                console.log("CRAB RAVE");
                //testcallback();
                if(i === resultsList.length){
                    displayResults();
                }
            }).fail(function(){
                alert("Could not search by author - 4.");
                loading = false;
            });
        }
        //callback();
    } else {
        results.innerHTML = "<p style=\"text-align: center;\">No results found!</p>";
    }
}


function displayResults() {
	var i = 0, resultsStr = "";
	console.log(resultsList);
    console.log("THE ONLY RESULTS CALL WE SHOULD BE GETTING:");
    //console.log(authorList);
    console.log(authorDict);
    console.log(resultsList);
    console.log("======================================================");
    if (resultsList.length > 0) {
    	for (i = 0; i < resultsList.length; i++) {
    		resultsStr +=   "<div class=\"returned-comic-wrapper\">"
    		            +       "<div class=\"returned-comic\">"
    		            +       "<input style=\"width:100%;font-size:12px;\" type=\"submit\" readonly=\"true\" placeholder=\""
    		            +       ((resultsType == "Series") ? resultsList[i].seriesID : resultsList[i].comicId)
    		            +       "\" value=\"" + resultsList[i].title + "\" onclick=\"getRequestedSeriesId(this)\">"
    		            +       "<div style=\"font-size:12px;\"><br>By: " + authorDict[resultsList[i].userId]+ "<br>"
                        +       resultsList[i].description + "</div></div></div>";

            /* console.log(authorList[i]); */
    	}
        results.innerHTML = resultsStr;
	} else {
	    results.innerHTML = "<p style=\"text-align: center;\">No results found!</p>";
	}
}

function getRequestedSeriesId(e) {
	if (resultsType == "Series") {
        sessionStorage.setItem("requestedSeriesID", e.placeholder);
        sessionStorage.setItem("requestedComicID", "none");
	} else if (resultsType == "Comic") {
		var i = 0;
		for (i = 0; i < resultsList.length; i++) {
			if (resultsList[i].comicId == e.placeholder) {
				sessionStorage.setItem("requestedSeriesID", resultsList[i].seriesId);
				break;
			}
		}
		if (i == resultsList.length) { sessionStorage.setItem("requestedSeriesID", null); }
		sessionStorage.setItem("requestedComicID", e.placeholder);
	}
}

/* When document is done loading, initialize search page. */
window.onload = function() {
	/* Retrieve all HTML control elements. */
    titleInput 		= document.getElementById("titleInput");
    authorInput 	= document.getElementById("authorInput");
    tagInput		= document.getElementById("tagInput");
    searchButton 	= document.getElementById("searchButton");
    results 		= document.getElementById("results");
    searchType		= document.getElementById("searchType");
	
	/* Clear results list. */
	results.innerHTML = "";
	
	/* Set control handlers. */
	searchButton.onclick = search;
};