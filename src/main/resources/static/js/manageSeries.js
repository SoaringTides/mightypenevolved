/*
 *
 * authors: Benjamin Shu, Peter Ly, Stefania Zimmerman
 *
 */

/* Initialize variables for storing Series data. */
var userBox, username, user, seriesList, series, seriesID, title;

/* Initialize variables for HTML control elements. */
var editButton, addButton, deleteButton, titleInput, seriesDisplay, changeButton;
var tagOverlay, tagInput, tagsList;

/* Initialize Boolean lock for database requests. */
var loading = false;

var currentUser;
var currentUserId;
var userResultsHolder;
var currentSeriesHolder;

function loadUser(callback) {
    /* Retrieve information on current User. */
    username = userBox.innerHTML;
    console.log(username);
    loading = true;
    user = $.get("/byUser/userName/" + username, function(data) {
        console.log(data);
        loading = false;
    }).fail(function() {
        console.log("Could not load User information.");
        loading = false;
    });
}

function loadSeries(callback) {
	if (!loading) {
		/* Request every Series associated with the user. */
        loading = true;
        $.get('/byUser/series/' + userID, function(data) {
            seriesList = data;
            loading = false;
            callback();
        }).fail(function() {
            console.log("Could not load series data.");
            loading = false;
        });
	}
}
function displaySeries() {
	seriesDisplay.innerHTML = "";
	var seriesStr = "";
	var i = 0;
	for (i = 0; i < seriesList.length; i++) {
		seriesStr += "<div class=\"returned-comic-wrapper\"><div class=\"returned-comic\">" + seriesList[i].title + "</div></div>";
	}
}

function toggleOverlay(){
    //window.alert("Test");
    var div = document.getElementById('edit-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";

      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}

function toggleOverlayAdd(){
    //window.alert("Test");
    var div = document.getElementById('add-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";

      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}

function toggleOverlayDelete(){
    //window.alert("Test");
    var div = document.getElementById('delete-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";

      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}

var unparentedresults;
function toggleOverlayAddComic(){
    //window.alert("Test");
    var div = document.getElementById('addcomic-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
    if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";


        $.get('/getUnparentedComics', function(data) {
            unparentedresults = data;
            loading = false;
            updateComicChooser();
        }).fail(function() {
            console.log("Could not search by userid.");
            loading = false;
        });

    }
    else {
        div.style.display = "none";
        div2.style.display = "block";
    }
}
function updateComicChooser(){
    var i = 0;
    resultsStr ="<select id=\"comicSelector\">";
    for (i = 0; i < unparentedresults.length; i++) {

        addString = "<option value=\""
            + unparentedresults[i].title
            + "\">" + unparentedresults[i].title+ "</option>";

        resultsStr+=addString;

    }
    resultsStr+="</select>" + "<br>";
    document.getElementById("availableComics").innerHTML = resultsStr;
}

function addComicProdecure(){
    comicSelectorValue = document.getElementById("comicSelector").value;
    addComicToGivenSeries(comicSelectorValue);
}

function updateComicChooserDeprecated(){
    var i = 0, resultsStr = "<div class=\"dropdown\">\n" +
        "  <button onclick=\"toggleComicDrop()\" class=\"dropbtn\">Select a Comic</button>";
    console.log("THE ONLY unparentedresults CALL WE SHOULD BE GETTING:");
    console.log(unparentedresults);
    resultsStr +="<div id=\"comicDropDown\" class=\"dropdown-content\">";
    for (i = 0; i < unparentedresults.length; i++) {

        /*
        resultsStr += "<div class=\"returned-comic-wrapper\"><div class=\"returned-comic\">"
            + unparentedresults[i].comicId
            + "<br>Title:" + unparentedresults[i].title
            + "<br>Desc.:" + unparentedresults[i].description + "</div></div>";
         */


        //note these buttons trigger a divs.click at the bottom

        resultsStr +=   "<div class=\"returned-comic-wrapper\">"
            +       "<div class=\"returned-comic\">"
            +       "<input type=\"submit\" readonly=\"true\" placeholder=\"" + unparentedresults[i].comicId + "\" value=\"" + unparentedresults[i].title + "\" onclick=\"addComicToGivenSeries(this.value)\">"
            +       "<br>"
            +       unparentedresults[i].description + "</div></div>";


    }
    resultsStr+="</div></div>";
    document.getElementById("availableComics").innerHTML = resultsStr;
    var div = document.getElementById('comicDropDown');
    div.style.display = "none";

    /*
    document.getElementById("results").innerHTML = "<div class=\"returned-comic\">" + results.title + "<br>"
    + results.description;

     */
}


function toggleComicDrop() {
    var div = document.getElementById('comicDropDown');
    //window.alert(div.style.display);
    if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
    }
    else {
        div.style.display = "none";
    }
}

function addComicToGivenSeries(comicTitleToAdd){

    if (currentSeriesHolder != null && currentSeriesHolder != undefined) {
        console.log(comicTitleToAdd);
        //targetSeriesTitle = document.getElementById("addComicToSeriesName");

        //$.post('/Series/insertComicIntoSeries/' + targetSeriesTitle.value + '/' + comicTitleToAdd, function() {
        $.post('/Series/insertComicIntoSeries/' + currentSeriesHolder.title + '/' + comicTitleToAdd, function() {
            console.log("Page has made a post-add comic to series call!");
            //emptyPost();
            window.open ('manageSeries','_self',false);
            //location.reload();
        }).fail(function() {
            console.log("Could not add comic to series.");
        });
        //add comic id to series
    } else {
        document.getElementById("errorSaveMessage5").innerHTML = "Please select a series below first."
    }
}

function deleteSeries() {
    console.log("delete series");
    deletedname = document.getElementById("deleteseriesname");
    //console.log(deletedname);


    if(deletedname.value == ""){
        document.getElementById("errorSaveMessage3").innerHTML = "Please enter a series name."
    }
    else{
        $.post('/Series/deleteSeriesByUserIdScan/' + deletedname.value, function() {
            console.log("Page has made a post-delete call!");
            //emptyPost();
            window.open ('manageSeries','_self',false);
            //location.reload();
        }).fail(function() {
            console.log("Could not upload new Page.");
        });
    }
}

function toggleOverlayTag() {
    var div = document.getElementById('tag-overlay');
    var div2 = document.getElementById('menu-options');

    if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";
    } else {
        div.style.display = "none";
        div2.style.display = "block";
    }
}
function addTag() {
    if (currentSeriesHolder != null && currentSeriesHolder != undefined) {
        var name = document.getElementById('addtag').value;
        if (name != null && name != undefined && name != "") {
            console.log("Adding " + name);
            tagsList.innerHTML += "<button onclick=\"removeTag(this)\">X " + name + "</button>";

        } else {
            console.log("Name is null/undefined!");
        }
    } else {
        console.log("No series selected!");
    }
}
function removeTag(button) {
    tagsList.removeChild(button);
}
function saveTags() {
    var buttons = tagsList.childNodes, newTags = [], i = 0;
    for (i = 0; i < buttons.length; i++) {
        newTags[i] = buttons[i].innerHTML.substring(2);
    }
    if (newTags.length == 0) { newTags.push("eMpTy"); }
    else if (newTags[0] == "eMpTy") {
        newTags.push(newTags.shift());
    }
    console.log(newTags);
    $.post("/Series/UpdateSeriesTags/" + currentSeriesHolder.seriesID + "/" + newTags, function() {
        console.log("Updated series tags!");
    }).fail(function(){
        console.log("Could not update!");
    });
}

////////////////////////////////////////////////////////////////////
function searchByUserName(callback) {
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        console.log("Searching to get pages of a user...");
        /* Query database using user-entered title. */
        loading = true;
        // $.get('/Series/title/' + title, function(data) {
        //$.get('/byUser/userName/' + title, function(data) {
        console.log(currentUser);
        $.get('/byUser/userName/' + currentUser, function(data) {
            userResultsHolder = data;
            console.log(userResultsHolder);
            console.log(userResultsHolder.userID);
            loading = false;
            callback();
        }).fail(function() {
            console.log("Could not get user.");
            loading = false;
        });
    }
}


function getActualSeries(callback) {
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        console.log("Searching to get comics based on userid...");

        $.get('/Series/seriesUserId/' + userResultsHolder.userID, function(data) {
            results = data;
            loading = false;
            callback();
        }).fail(function() {
            console.log("Could not search by userid.");
            loading = false;
        });


    }
}

function search() {
    //event.preventDefault();
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        /* Load input from HTML page into variables. */
        usernameValue = currentUser;

        console.log("username: " + usernameValue);

        searchByUserName(function() {
            getActualSeries(function() {
                displayResults();
            });
        });
    }
}

function displayResults() {
    var i = 0, resultsStr = "";
    console.log(results);
    console.log("THE ONLY RESULTS CALL WE SHOULD BE GETTING:");
    var publishedstatus = "No";
    for (i = 0; i < results.length; i++) {
        if(results[i].published == true){
            publishedstatus = "Yes"
        }
        else{
            publishedstatus = "No"
        }
        resultsStr += "<div class=\"returned-comic-wrapper\"><div class=\"returned-comic\">"
                    + results[i].seriesID
                    + "\nTitle:" + results[i].title
                    + "<br>Desc.:" + results[i].description
                    + "<br>Published:" + publishedstatus
            + "</div></div>";
    }
    document.getElementById("seriesDisplay").innerHTML = resultsStr;
    /*
    document.getElementById("results").innerHTML = "<div class=\"returned-comic\">" + results.title + "<br>"
    + results.description;

     */
}

var descSeriesHolder;
function changeDescription() {
    newdesc = document.getElementById("newdesc");
    //seriesname = document.getElementById("seriesname");
    seriesnewname = document.getElementById("seriesnewname");
    //if(seriesname.value == ""){
    if(currentSeriesHolder == null){
        document.getElementById("errorSaveMessage").innerHTML = "Please select a series to update."
    }
    else{

        console.log("hi, inside else case of changedescription");
        $.get('/Series/title/' + currentSeriesHolder.title, function(data) {
            descSeriesHolder = data;
            console.log(descSeriesHolder);
            console.log(descSeriesHolder.title);
            finishDescriptionUpdate(newdesc,seriesnewname);
        }).fail(function() {
            console.log("Could not get series.");
        });
        /*
        $.post('/updateSeriesDescriptionFlexible/' + seriesname.value + '/' + newdesc.value + '/' + seriesnewname.value, function() {
            console.log("Page has made a post call!");
            window.open ('manageSeries','_self',false);
            //location.reload();
        }).fail(function() {
            console.log("Could not upload new Page.");
        });
         */
    }
};

function finishDescriptionUpdate(newdesc,seriesnewname) {
    newDescription = newdesc.value;
    newName = seriesnewname.value;
    console.log("VALUES BEFORE");
    console.log(newDescription)
    console.log(newName)

    if(seriesnewname.value == ""){
        newName = descSeriesHolder.title;
    }

    if(newdesc.value== ""){
        newDescription = descSeriesHolder.description;
    }

    console.log("VALUES AFTER");
    console.log(newDescription)
    console.log(newName)

    $.post('/updateSeriesDescriptionFlexible/' + currentSeriesHolder.title + '/' + newDescription + '/' + newName, function() {
        console.log("Page has made a post call!");
        //emptyPost();
        window.open ('manageSeries','_self',false);
        //location.reload();
    }).fail(function() {
        console.log("Could not upload new Page.");
    });
};

function publishComic(){

    $.post('/SDC/PublishSeries/' + currentSeriesHolder.title, function() {
        console.log("Page has made a PUBLISH call!");
        //emptyPost();
        window.open ('manageSeries','_self',false);
        //location.reload();
    }).fail(function() {
        console.log("Could not publish new Page.");
    });
}
////////////////////////////////////////////////////////////////////
//Needed to select comic block on click
//window.alert("test one");
$(document).ready(function() {
    //window.alert("test two");
  $Divs = $(".returned-comic");

  $Divs.click(function() {
    //window.alert("click recorded")
    $Divs.removeClass("highlight");
    $(this).addClass("highlight");
    var seriesTitle = $(event.target).text();
    console.log(seriesTitle);
    //window.alert("Currently selected series is "+seriesTitle);
  });
});

window.onload = function() {
    /* Retrieve information on current User. */
    userBox         = document.getElementById("username");

    /* Retrieve all HTML control elements. */
    editButton		= document.getElementById("editButton");
    addButton		= document.getElementById("addButton");
    deleteButton	= document.getElementById("deleteButton");
    titleInput		= document.getElementById("titleInput");
    changeButton    = document.getElementById("changeButton");

    /* Retrieve Series display object. */
    seriesDisplay   = document.getElementById("seriesDisplay");
    seriesDisplay.innerHTML = "";

    /* Retrieve tag editing overlay. */
    tagOverlay      = document.getElementById("tag-overlay");
    tagInput        = document.getElementById("addtag");
    tagsList        = document.getElementById("tagslist");

    /* Initialize handlers for control elements. */
    //addButton.onclick 		= addSeries;
    deleteButton.onclick	= deleteSeries;
    changeButton.onclick = changeDescription;

    //window.alert("test two");
    $Divs = $(".search-results");

    $Divs.click(function() {
        //window.alert("click recorded")
        //$Divs.removeClass("highlight");
        //$(this).addClass("highlight");
        if ($(event.target)[0] === $Divs[0]) { return; }
        var seriesTitle = $(event.target).text();
        var seriesId = seriesTitle.substring(0, seriesTitle.indexOf("\n"));
        console.log(seriesId);
        var index = seriesTitle.lastIndexOf("Desc.:");
        seriesTitle = seriesTitle.substring(6,index);
        //window.alert("Currently selected series is "+seriesTitle);
        document.getElementById("seriesTitle").innerHTML = seriesTitle;
        if (tagOverlay.display != "none") {
            tagsList.innerHTML = "";
            $.get("/Series/seriesId/" + seriesId, function(data) {
                console.log("Got series!");
                console.log(data);
                currentSeriesHolder = data;
                var i = 0;
                if(data.tag!=null){
                    for (i = 0; i < data.tag.length; i++) {
                        tagsList.innerHTML += "<button onclick=\"removeTag(this)\">X " + data.tag[i] + "</button>";
                    }
                }
            });
        }
    });

    //loadUser();
    currentUser = document.getElementById("username").innerHTML;
    search();
}
