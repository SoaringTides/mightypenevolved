/*
 *
 * authors: Benjamin Shu, Peter Ly, Stefania Zimmerman
 *
 */

/* Add Boolean lock to wait for images from database. */
var loading = false;

/* Retrieve HTML control elements for navigating pages. */
var nextButton, prevButton, choiceButtons;

/* Set up control variables for requesting pages from database. */
var userBox, username, user,
    seriesID, series,
    comicID, comic, comicIndex = 0,
    pageID, page, prevPages = [ ];

var subscribeButton;

/* Initialize global variable for the Fabric.js canvas. */
var canvas;

var title, subtitle;


//corresponds to the HTML div where generated choice buttons are held.
var choiceHolder;

/* Temporary variable for loading file input. */
var loadFileInput;
var testPageLoad;

function subscribeToSeries() {
	if (!user.subscriptions) { user.subscriptions = new Array(); }
	if (user.subscriptions.indexOf(series) < 0) {
		user.subscriptions.push(series.seriesID);
		console.log(user);
		$.post('/addSubscription/' + user.userID + '/' + series.seriesID, function(){
			console.log("Updated user successfully!");
		}).fail(function(){
			console.log("Could not update user subscriptions!");
		});
	}
}

function getUserData(callback) {
    if (!loading) {
        loading = true;
        /* Load User data from database. */
        $.get('/byUser/userName/' + username, function(data) {
            console.log(data);
            user = data;
            loading = false;
            callback();
        }).fail(function(){
            console.log("Could not get user data!");
            loading = false;
        });
    }
}

function getSeriesData(callback) {
    if (!loading) {
        loading = true;
        /* Load series data from database. */
        $.get('/Series/seriesId/' + seriesID, function(data) {
            /* Store series in local object and set current comic ID to first comic. */
            console.log(data);
            series = data;
            console.log("Before: " + comicID);
            if (comicID == "none") { comicID = series.comicID[0]; }
            console.log("After: " + comicID);
            loading = false;
            callback();
        }).fail(function(){
            console.log("Could not get series data!");
            loading = false;
        });
    }
}

/* Retrieve every Comic in a Series and store it in a local array. */
function getComicData(callback) {
	if (!loading) {
        loading = true;
        /* Retrieve comic information using current ID. */
        $.get('/Comics/comicId/' + comicID, function(data) {
            console.log(data);
            comic = data;
            loading = false;

            if (comic.startPage) { console.log("Updating page ID..."); pageID = comic.startPage; }
            else { pageID = comic.pageId[0]; }

            callback();
        }).fail(function(){
            console.log("Could not get comic data!");
            loading = false;
        });
    }
}

function getNextComic(callback) {
    if (!loading) {
        if (comicIndex < series.comicID.length - 1) {
            loading = true;
            comicIndex++;
            comicID = series.comicID[comicIndex];

            callback();
        } else {
            console.log("There are no more comics in this Series!");
        }
    }
}

function getPrevComic(callback) {
    if (!loading) {
        if (comicIndex > 0) {
            loading = true;
            comicIndex--;
            comicID = series.comicID[comicIndex];

            callback();
        } else {
            console.log("This is the first comic in the series!");
        }
    }
}

function getPageData(callback) {
    if (!loading) {
        loading = true;
        /* Retrieve current page as a JSON. */
        console.log("THIS IS THE PAGE ID");
        console.log(pageID);
        $.get('/Page/' + pageID, function(data) {
            console.log(data);
            page = data;
            loading = false;
            callback();
        }).fail(function(){
            console.log("Could not get page data!");
            loading = false;
        });
    }
}

function getNextPage(callback) {
    if (!loading) {
        if (page.choicePages.length == 1) {
            prevPages.push(pageID);
            pageID = page.choicePages[0];
            console.log(pageID);

            getPageData(function(){
                displayPage();
            });
        } else if (page.choicePages.length == 0) {
            if (comicIndex + 1 < series.comicID.length) {
                comicIndex++;
                comicID = series.comicID[comicIndex];
                prevPages = new Array();
                getComicData(function() {
                    getPageData(function() {
                        displayPage();
                    });
                });
            }
        }
    }
}

function getPrevPage(callback) {
    if (!loading) {
        if (prevPages.length > 0) {
            pageID = prevPages.pop();

            getPageData(function() {
                displayPage();
            });
        } else if (prevPages.length == 0) {
            if (comicIndex - 1 >= 0) {
                comicIndex--;
                comicID = series.comicID[comicIndex];
                getComicData(function(){
                    getPageData(function(){
                        displayPage();
                    });
                });
            }
        }
    }
}

function displayPage() {
    console.log("THIS IS THE TARGET PAGE");
    if (page != null) {
        console.log(page);
        prevButton.disabled =   (prevPages.length == 0) &&
                                (comicIndex < 0);
        nextButton.disabled =   (page.choicePages.length != 1) &&
                                (page.choicePages.length != 0) &&
                                (comicIndex >= series.comicID.length);

        console.log(prevPages);
        console.log(comic);
        console.log(page);

        title.innerHTML = comic.title;
        subtitle.innerHTML = series.title;

        console.log(comic.title);
        console.log(series.title);

        canvas.clear();
        var json = JSON.parse(page.image);
        canvas.loadFromJSON(json, canvas.renderAll.bind(canvas));
        initializeChoiceButtons();
    }
}


function initializeChoiceButtons(){

    resultsStr="No choices found!"
    if(page.choicePages != null && page.choicePages.length > 1) {
        resultsStr="";
        //console.log("ALL THE CHOICES OF THIS PAGE");
        //console.log(page);
        //console.log(page.choicePages);
        //console.log(typeof(page.choicePages));
        page.choicePages.forEach(function(element) {
            console.log(element);
            //resultsStr += "<div class=\"returned-comic-wrapper\"><div class=\"returned-comic\">" + element + "</div></div>";


            resultsStr +=   "<div class=\"returned-choice-wrapper\">"
                +       "<div class=\"returned-choice\">"
                +       "<input type=\"submit\" readonly=\"true\" placeholder=\"" + element
                +       "\" value=\"" + element
                +       "\" onclick=\"loadPage(this.value)\">"
                +       "</div></div>";

        });
        document.getElementById("results").innerHTML = resultsStr;

    } else {
        document.getElementById("results").innerHTML = "";
    }
}

function loadPage(viewpageId){
    if (!loading) {
        //be sure to set checks for if the choice jumps to the end/begnning of the comic
        prevPages.push(page.id);
        console.log(prevPages);

        //get the comic page that has this id
        //get the index of this comic's page, and set it accordingly
        //this assumes it's a page in the same comic
        //console.log(comic);
        //console.log("page id before");
        //console.log(comic.pageId);

        //note - iterating by comic.pageId.length doesn't work because it confuses JS and makes it think
        //it's looking at a string - only the first string in the array.
        comic.pageId.forEach(function(element) {
            if(element == viewpageId){
                pageID = viewpageId;
            }
        });

        //console.log("page ID after");
        //console.log(pageID);

        //Uncaught SyntaxError: Unexpected token u in JSON at position 0
        //some variable is not filled out properly....

        getPageData(function(){
            displayPage();
        });
    }
}

/* Initalize page controls after all assets have loaded. */
window.onload = function() {
    /* Retrieve user information. */
    userBox = document.getElementById("userBox");

	/* Retrieve all control elements from HTML page. */
	nextButton	= document.getElementById("nextButton");
	prevButton	= document.getElementById("prevButton");

	/* Set up page control handlers. */
	nextButton.onclick	= getNextPage;
	prevButton.onclick	= getPrevPage;

	/* Initialize static canvas for viewing page JSON. */
	canvas = new fabric.StaticCanvas("pageDisplay");
	title       = document.getElementById("title");
	subtitle    = document.getElementById("subtitle");

	subscribeButton = document.getElementById("subscribe");
	subscribeButton.onclick = subscribeToSeries;

	/* Retrieve ID for requested User and Series. */
	username = userBox.innerHTML;
	/*safe assumption - sessionStorage will always be initalized since we always come from search*/
	seriesID = sessionStorage.getItem("requestedSeriesID");
	comicID = sessionStorage.getItem("requestedComicID");
	console.log(comicID);

	/*get the */
    choiceHolder = document.getElementById("choiceholder");
	
	/* Start loading the first page of the Series. */
	getUserData(function(){
	    getSeriesData(function() {
	        getComicData(function() {
	            getPageData(function() {
                    displayPage();
                });
	        });
	    });
	});
};