/*
 *
 * authors: Benjamin Shu, Peter Ly, Stefania Zimmerman
 *
 */

/* Set up Boolean lock to wait for database response. */
var loading = false;

/* Set up control variables for validating user input. */
var user, pass, firstName, lastName;

/* Initialize variables for HTML control elements. */
var notice, userInput, passInput, firstNameInput, lastNameInput, submitButton;

/* String constants for UI messages. */
const BLANK_USER = "Entering a username is required.",
    BLANK_PASS = "Entering a password is required.",
    INVALID_USER = "Please enter a valid username.",
    INVALID_PASS = "Please enter a valid password.",
    TAKEN_USER = "That username is already taken.",
    LOADING = "Registration in progress...";

/* Function for checking the user-entered fields. */
function checkInput() {
	/* Load input values from HTML page. */
	//userInput = user.value;
	//passInput = pass.value;
	//firstName = firstNameInput.value;
	//lastName = lastNameInput.value;

	/* If any required field is blank, inform user. */
	if (userInput == "") {
        //notice.innerHTML = BLANK_USER;
	    return;
    } else if (passInput == "") {
        //notice.innerHTML = BLANK_PASS;
        return;
    }
	
	/* Check that username is not already taken. */
	// $.get('/byUser/username/' + userInput, function(data) {
	//	
	// });
	
	/* If input is valid, call registerUser(). */
	//registerUser();
}

function registerUser() {
	/* Send created user object to the database. */
}

window.onload = function() {
	/* Retrieve all control elements from HTML page.*/
};