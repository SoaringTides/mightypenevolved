var userBox, userName = "", userID = "", user,
	subsList, subsDisplay,
	seriesID = "", unsubInput, unsubButton;

var loading = false;

function getUserData(callback) {
	if (!loading) {
		loading = true;

		/* Load User data from database. */
	    $.get('/byUser/userName/' + userName, function(data) {
	        console.log(data);
	        user = data;
	        userID = user.userID;
	        subsList = user.subscriptions;
	        loading = false;
	        callback();
	    }).fail(function(){
	        console.log("Could not get user data!");
	        loading = false;
	    });
	}
}

function displayResults() {
	var i = 0, resultsStr = "", remaining = subsList.length;
	subsDisplay.innerHTML = "";
	loading = true;
	for (i = 0; i < subsList.length; i++) {
		$.get('/Series/seriesId/' + subsList[i], function(data) {
			resultsStr  +=  "<div class=\"returned-comic-wrapper\" onclick=\"selectSeriesTitle(this)\""
						+   " title=\"" + data.title + "|" + data.seriesID + "\">"
                        +   "<div class=\"returned-comic\">"
                        +   "<input style=\"width:100%;font-size:12px;\" type=\"submit\" readonly=\"true\" placeholder=\""
                        +   data.seriesID + "\" value=\"" + data.title + "\" onclick=\"getRequestedSeriesId(this)\">"
                        +   "<div style=\"font-size:12px;\"><br>"
                        +   data.description + "</div></div></div>";
            remaining -= 1;
            if (remaining == 0) {
                loading = false;
                console.log("Done loading all subscriptions!");
                subsDisplay.innerHTML = resultsStr;
            }
		}).fail(function() {
			console.log("Could not get subscription at index " + i);
			remaining -= 1;
			if (remaining == 0) {
				loading = false;
				console.log("Done loading all subscriptions!");
				subsDisplay.innerHTML = resultsStr;
			}
		});
	}
}

function selectSeriesTitle(e) {
	var info = e.title, index = info.indexOf("|");
	seriesTitle = info.substring(0, index);
	seriesID = info.substring(index + 1);
	unsubInput.value = seriesTitle;
	console.log(seriesTitle);
	console.log(seriesID);
}

function getRequestedSeriesId(e) {
	sessionStorage.setItem("requestedSeriesID", e.placeholder);
	sessionStorage.setItem("requestedComicID", "none");
}

function unsubscribe() {
	if (seriesID != "") {
		var ind = subsList.indexOf(seriesID);
    	if (ind >= 0) {
    		$.post('/removeSubscription/' + userID + '/' + seriesID, function() {
    			console.log("Successful unsubscription!");
    			subsList.splice(ind, 1);
    			console.log(subsList);

    			seriesID = "";
    			seriesTitle = "";
    			unsubInput.value = "";

    			displayResults();
    		}).fail(function(){
    			console.log("Could not unsubscribe!");
    		});
    	}
	}
}


window.onload = function() {
	subsDisplay     = document.getElementById("subsDisplay");
	unsubInput      = document.getElementById("unsubInput");
	unsubButton     = document.getElementById("unsubButton");
	userBox         = document.getElementById("userBox");

	userName = userBox.innerHTML;
	unsubButton.onclick = unsubscribe;

	getUserData(function() {
		displayResults();
	});
}
