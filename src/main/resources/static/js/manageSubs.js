/*
 *
 * authors: Benjamin Shu, Peter Ly, Stefania Zimmerman
 *
 */

/* Initialize variables for storing page data. */
var userID, user, userSeries, series, comicList, comic, pages, page;

/* Initialize variables for HTML control elements. */
var seriesList, comicList, pagesList,
	indexInput, choiceA, choiceB,
	addButton, deleteButton;

/* Initialize Boolean lock for database requests. */
var loading = false, loadCount = 0;

var currentUser;
var currentUserId;
var userResultsHolder;

/*Functions for overlays*/

function toggleOverlayDelete(){
    //window.alert("Test");
    var div = document.getElementById('delete-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";

      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}

function toggleOverlay(){
    //window.alert("Test");
    var div = document.getElementById('add-overlay');
    var div2 = document.getElementById('menu-options');
    //window.alert(div.style.display);
      if (!div.style.display || div.style.display == "none") {
        div.style.display = "block";
        div2.style.display = "none";
        generateChoiceDivs();
      }
      else {
        div.style.display = "none";
        div2.style.display = "block";
      }
}

function loadSeriesList(callback) {
	if (!loading) {
		/* Retrieve the list of series associated with user. */
        loading = true;
        $.get('/byUser/series/' + userID, function(data) {
            userSeries = data;
            loading = false;
            callback();
        }).fail(function(){
            console.log("Could not load user's series content.");
            loading = false;
        });
	}
}
function updateSeries() {
	/* Update selection of series. */
	seriesList.innerHTML = "";
	seriesStr = "";
	var i = 0;
	for (i = 0; i < userSeries.length; i++) {
		seriesStr += "<option value=\"" + userSeries[i].title + "\">" + userSeries[i].title + "</option>";
	}
}

function loadComicList(callback) {
	if (!loading) {
		/* Retrieve all comics associated with currently selected series. */
        loading = true;
        $.get('/series/' + series.seriesId + '/comics', function(data) {
            comics = data;
            loading = false;
            callback();
        }).fail(function() {
            console.log("Could not load comics for current series.");
            loading = false;
        });
	}
}
function updateComics() {
	/* Update selection of comics. */
	comicsList.innerHTML = "";
	comicsStr = "";
	var i = 0;
	for (i = 0; i < comics.length; i++) {
		comicsStr += "<option value=\"" + comics[i].title + "\">" + comics[i].title + "</option>";
	}
}


function updatePages() {
	/* Update selection of comics. */
	pagesList.innerHTML = "";
	pagesStr = "";
	var i = 0;
	for (i = 0; i < pages.length; i++) {
		pagesStr += "<option value=\"" + pages[i].pageNumber + "\">" + pages[i].pageNumber + "</option>";
	}
}

////////////////////////////////////////////////////////////////////
function searchByUserName(callback) {
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        console.log("Searching to get pages of a user...");
        /* Query database using user-entered title. */
        loading = true;
        // $.get('/Series/title/' + title, function(data) {
        //$.get('/byUser/userName/' + title, function(data) {
        console.log(currentUser);
        $.get('/byUser/userName/' + currentUser, function(data) {
            userResultsHolder = data;
            console.log(userResultsHolder);
            console.log(userResultsHolder.userID);
            loading = false;
            callback();
        }).fail(function() {
            console.log("Could not get user.");
            loading = false;
        });
    }
}

//get every page beloning to user, put values into dropdown
function generateChoiceDivs(){
    getActualPages(function (){
        generateChoiceDivsHelper();
    });
}

function generateChoiceDivsHelper(){
    var i = 0;
    var resultsStrINP = "";
    var resultsStrINCP = "";
    var resultsStrREMIP = "";
    var resultsStrREMCP = "";
    resultsStrREMCP+="<select id=\"choicePageInput_Remove\">";

    for (i = 0; i < results.length; i++) {

        addString = "<option value=\""
            + results[i].id
            + "\">" + results[i].id+ "</option>";

        resultsStrINP+=addString;
        resultsStrINCP+=addString;
        resultsStrREMIP+=addString;
        resultsStrREMCP+=addString;

    }
    resultsStrINP+="</select>" + "<br>";
    resultsStrINCP+="</select>" + "<br>";
    resultsStrREMIP+="</select>" + "<br>";
    resultsStrREMCP+="</select>" + "<br>";

    document.getElementById("removeChoiceDiv").innerHTML = resultsStrREMIP + resultsStrREMCP;
}

function getActualPages(callback) {
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        console.log("Searching to get comics based on userid...");

        $.get('/Page/pageUserId/' + userResultsHolder.userID, function(data) {
            results = data;
            loading = false;
            callback();
        }).fail(function() {
            console.log("Could not search by userid.");
            loading = false;
        });


    }
}

function search() {
    //event.preventDefault();
    /* Only attempt search if not already waiting for data. */
    if (!loading) {
        /* Load input from HTML page into variables. */
        usernameValue = currentUser;

        console.log("username: " + usernameValue);

        searchByUserName(function() {
            getActualPages(function() {
                displayResults();
            });
        });
    }
}

function displayResults() {
    var i = 0, resultsStr = "";
    console.log(results);
    console.log("THE ONLY RESULTS CALL WE SHOULD BE GETTING:");
    for (i = 0; i < results.length; i++) {
        resultsStr += "<div class=\"returned-comic-wrapper\"><div class=\"returned-comic\">" + "Page Id: "  + results[i].id
            + "<br>" + "Page Number: " + results[i].pageNumber +
            "<br>" + "Choices: " + results[i].choicePages.toString() +"</div></div>";
    }
    document.getElementById("results").innerHTML = resultsStr;
    /*
    document.getElementById("results").innerHTML = "<div class=\"returned-comic\">" + results.title + "<br>"
    + results.description;

     */
}



function addIndices() {
    if(parentPage.value == "" || choicePage.value == ""){
        document.getElementById("errorSaveMessageChoices").innerHTML = "Please fill out both fields!."
    }
    else{
        $.post('/Page/AddChoice/' + parentPage.value + '/' + choicePage.value, function() {
            window.open ('managePage','_self',false);
        }).fail(function() {
            console.log("Could not link pages");
            document.getElementById("errorSaveMessageChoices").innerHTML="Error when trying to link pages. " +
                "Make sure they are in the same Comic.";
        });

    }
}


function removeIndices() {
    choicePage = document.getElementById("choicePageInput_Remove");
    if(parentPage.value == "" || choicePage.value == ""){
        document.getElementById("errorRemoveMessageChoices").innerHTML = "Please fill out both fields!."
    }
    else{
        $.post('/Page/RemoveChoice/' + parentPage.value + '/' + choicePage.value, function() {
            window.open ('managePage','_self',false);
        }).fail(function() {
            console.log("Could not link pages");
        });

    }
}


var holdPageInfo;
var holdComicInfo;

function deletePage(){

    deletedname = document.getElementById("removeSub");
    //console.log(deletedname);


    if(deletedname.value == ""){
        document.getElementById("errorSaveMessagePageDel").innerHTML = "Please enter a username."
    }
    else{
        $.get('/Page/' + deletedname.value, function(data) {
            console.log("Page has made a post-delete call! 1");
            holdPageInfo = data;
            deletePageAcquireComic()

        }).fail(function() {
            console.log("Could not get page's data.");
        });
    }

};

function deletePageAcquireComic(){
    $.get('/Comics/comicId/' + holdPageInfo.comicId, function(data) {
        console.log("Page has made a post-delete call! 2");
        holdComicInfo = data;
        deletePagePerformDelete()

    }).fail(function() {
        console.log("Could not get comics's data.");
    });

}
function deletePagePerformDelete(){
    console.log("HPI + CPI");
    console.log(holdPageInfo);
    console.log(holdComicInfo);
    $.post('/Page/DeleteParameterizedPage/' + holdPageInfo.id + '/' + holdComicInfo.comicId, function(data) {
        console.log("Page has made a post-delete call! 3");
        window.open ('managePage','_self',false);

    }).fail(function() {
        console.log("Could not perform the deletion.");
    });


}

////////////////////////////////////////////////////////////////////



/* Initialize page when document loads. */
window.onload = function() {
	/* Retrieve all HTML control elements. */
    seriesList		= document.getElementById("seriesList");
    comicList		= document.getElementById("comicList");
    pagesList		= document.getElementById("pagesList");
    indexInput		= document.getElementById("indexInput");
    choiceA			= document.getElementById("choiceA");
    choiceB			= document.getElementById("choiceB");
    addButton		= document.getElementById("addButton");
    deleteButton	= document.getElementById("deleteButton");
    choicePageButton	= document.getElementById("choiceButton");
    choicePageButtonRemove	= document.getElementById("choiceButton_Remove");

    choicePageButton.onclick = addIndices;
    deleteButton.onclick = deletePage;
    choicePageButtonRemove.onclick = removeIndices;

    currentUser = document.getElementById("username").innerHTML;

	/* Set up handlers for control elements. */
    //seriesList.onchange = updateComics;
    //comicsList.onchange = updatePages;
    search();
};