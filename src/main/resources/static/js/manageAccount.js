/*
 *
 * authors: Benjamin Shu, Peter Ly, Stefania Zimmerman
 *
 */

/* Declare variables for HTML display elements. */
var userBox, emailBox, firstNameBox, lastNameBox;

/* Variable for storing the User object returned by the database. */
var user;

/* Variable for user's ID */
var userID;

/* Initialize Boolean lock for database requests. */
var loading = false, loadCount = 0;

/* Call this function when the page loads to load the current user's data. */
function loadUserInfo() {
	console.log("The value of userbox is " + userBox.innerHTML);
	userID = userBox.innerHTML;
		if (!loading) {
    		/* Request the currently signed in user */
            loading = true;
            $.get('/byUser/userName/' + userID, function(data) {
                user = data;
                loading = false;
                //For testing
                var email = user.emailAddress;
                var firstName = user.firstName;
                var lastName = user.lastName;
                //window.alert(email+firstName+lastName);
                emailBox.innerHTML = email;
                firstNameBox.innerHTML = firstName;
                lastNameBox.innerHTML = lastName;
            }).fail(function() {
                console.log("Could not load user's account information.");
                loading = false;
            });
    	}
}
//Function for checking that the input is valid
//Call this before changing the account values
//Field is what the input is, (email, firstname, lastname, etc)
//Returns true if the input is valid for that field
function validateInput(input,field){
    //Checks that necessary fields (username and password) are not null
    if(input ==' ' && (field == "username" || field == "password")){
        window.alert("New "+field+" cannot be null, please fill out this field.");
        return false;
    }
    //Checks that the email has both @ and . (stronger check might be implemented in the future
    if(field == 'email' && (input.includes("@") == false || input.includes(".") == false)){
        window.alert("Please enter a valid email.")
        return false;
    }
    return true;
}

/* Declare any necessary variables, functions, etc. here! */

/* Called when the page is done loading - this sets up every control element we need. */
window.onload = function() {
	/* Retrieve information for current User. */
	userBox = document.getElementById("username");
	emailBox = document.getElementById("email");
    firstNameBox = document.getElementById("firstName");
    lastNameBox = document.getElementById("lastName");

	/* When all page setup is done, call loadUserInfo(). */
	loadUserInfo();
};