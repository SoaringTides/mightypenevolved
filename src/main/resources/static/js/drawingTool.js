/*
 *
 * authors: Benjamin Shu, Peter Ly, Stefania Zimmerman
 *
 */

/* Define different states for mouse click handlers. */
/* Depending on the edit state, clicking the canvas may insert a line, circle, rectangle, etc. */
const 	NONE = "NONE",
    LINE = "LINE",
    CIRCLE = "CIRCLE",
    RECTANGLE = "RECTANGLE",
    BRUSH = "BRUSH",
    TEXT = "TEXT";

/* Variable for storing current edit state. */
var editState = NONE;

/* Boolean flags to be set while making jQuery calls. */
/* Certain functions will do nothing if these are set to true. */
var loading = false, saving = false;

/* Boolean flag for undoing changes. */
/* This needs to be set to true while undoing. */
var undoing = false;

/* Create array for tracking previous states. */
var states = new Array(), stateIndex = 0;

/* Define blank object and template for recording canvas states. */
var actionObj = {};
const actTemplate = {
    "type"			: "",
    "obj"			: null,
    "angle"			: 0.0,
    "left"			: 0,
    "top"			: 0,
    "fill"			: "",
    "strokeWidth"	: "",
    "width"			: "",
    "fontSize"		: 0,
    "fontFamily"	: "",
    "index"			: -1
};

/* Define action types for states array. */
/* When undoing, the function makes changes based on the type. */
const 	TYPE_ADD			= "add",
    TYPE_REMOVE			= "remove",
    TYPE_MOVE			= "move",
    TYPE_EDIT			= "edit";

/* Define string constants for user messages. */
const 	UNSAVED	= "Any unsaved changes will be lost. Are you sure you want to continue?",
    SAVING	= "Enter a name for your saved file:";

/* Set up control variables for editor. */
/*
 * canvas - the actual canvas that the user draws on
 */
var canvas;

/* Default file names for saved drawings. */
const currentFile = "drawing";

/* Control variables for downloading/uploading pages. */
var pageId, page;
/* JSON object template for uploading Pages to the database. */
const pageTemplate = {
    "id": "",
    "pages": [],
    "image": "",
    "pageNumber": -1
}

var currentUser;
var userResultsHolder;

//holds page info from sessionstorageobject retrieval
var holdPageInfo;


/*Functions for the overlay*/
var comicsList;
function toggleSaveOverlay(){
    //window.alert("2");
    var div = document.getElementById('save-drawing-overlay');
    //window.alert(div.style.display);
    if (div != null) {
        if ((!div.style.display || div.style.display == "none")) {
            div.style.display = "block";

            //1st callback
            $.get('/byUser/userName/' + currentUser, function(data) {
                userResultsHolder = data;
                console.log(userResultsHolder);
                console.log(userResultsHolder.userID);
                loading = false;

                //2nd callback
                $.get('/Comics/comicUserId/' + userResultsHolder.userID, function(data) {
                    comicsList = data;
                    loading = false;
                    updateComicChooser();
                }).fail(function() {
                    console.log("Could not search by userid.");
                    loading = false;
                });
            }).fail(function() {
                console.log("Could not get user.");
                loading = false;
            });

            /*
            $.get('/Comics/all', function(data) {
                allcomics = data;
                loading = false;
                updateComicChooser();
            }).fail(function() {
                console.log("Could not search by userid.");
                loading = false;
            });
             */
        }
        else {
            div.style.display = "none";
        }
    } else { console.log("toggleSaveOverlay() - variable 'div' is null or undefined."); }
}

function updateComicChooser(){
    var i = 0
    var resultsStr ="<select id=\"comicname\">";
    console.log("THE ONLY updateComicChooser CALL WE SHOULD BE GETTING:");
    if (comicsList) {
        for (i = 0; i < comicsList.length; i++) {

            addString = "<option value=\""
                + comicsList[i].title
                + "\">" + comicsList[i].title + "</option>";
            resultsStr+=addString;
        }
        resultsStr+="</select>" + "<br>";
        var comicAlterDiv = document.getElementById("comicAlterDiv");
        if (comicAlterDiv) { comicAlterDiv.innerHTML = resultsStr; }
    } else { console.log("updateComicChooser() - HTML element 'comicAlterDiv' does not exist."); }
    /*
    var resultsStr = "<div class=\"dropdown\">\n" +
        "  <button onclick=\"toggleComicDrop()\" class=\"dropbtn\">Select a Comic</button>";
    console.log("THE ONLY updateComicChooser CALL WE SHOULD BE GETTING:");
    console.log(comicsList);
    resultsStr +="<div id=\"comicDropDown\" class=\"dropdown-content\">";
    if (comicsList != null) {
        for (i = 0; i < comicsList.length; i++) {
            resultsStr +=   //"<div class=\"returned-comic-wrapper\">"
                //+       "<div class=\"returned-comic\">"
                //+
                    "<input type=\"submit\" readonly=\"true\" placeholder=\"" + comicsList[i].comicId + "\" value=\""
                + comicsList[i].title
                + "\" onclick=\"updateComicField(this.value)\">"
                +       "<br>"
                //+       comicsList[i].description + "</div>"
                //+ "</div>"
                ;
        }
    }

    resultsStr+="</div></div>";
    var available = document.getElementById("availableComics");
    if (available){ available.innerHTML = resultsStr; }
    var div = document.getElementById('comicDropDown');
    div.style.display = "none";
     */
	/*
    if (div) { div.style.display = "none"; }
    else { console.log("updateComicChooser() - variable 'div' is null or undefined."); }
    */
}

function updateComicField(valueInput){
    var name = document.getElementById("comicname");
    if (name) { name.value = valueInput; }
    else { console.log("updateComicField() - HTML element 'comicname' does not exist."); }
    toggleComicDrop();
}


function toggleComicDrop() {
    var div = document.getElementById('comicDropDown');
    //window.alert(div.style.display);
    if (div) {
        if (!div.style.display || div.style.display == "none") {
            div.style.display = "block";
        }
        else {
            div.style.display = "none";
        }
    } else { console.log("toggleComicDrop() - HTML element 'comicDropDown' does not exist."); }
}

var startX = 0, startY = 0, offsetX = 0, offsetY = 0;
var selected = false, deselected = false;
var undo, redo, del, line, circ, rect, brush, txt, img, loadImageInput;
var newButton, loadButton, loadFileInput, saveButton, downloadButton;

function setLineState() {
    if (editState == BRUSH) {
        if (fillColor) {
            fillColor.disabled = false;
            fillColor.value = '#FFFFFF';
        }
        if (strokeColor) {
            strokeColor.value = '#000000';
            strokeWidth.value = 3;
        }
    }
    editState = LINE;
    if (canvas) { canvas.isDrawingMode = false; }
    else { console.log("setLineState() - variable 'canvas' is null or undefined."); }
}
function addLine() {
    if (deselected) { deselected = false; selected = false; }
    else if (!selected) {
        if (canvas) {
            var point = canvas.getPointer(event.e), x = point.x, y = point.y;
            var obj = new fabric.Line([x - 37.5, y, x + 37.5, y], { fill: '#000000', stroke: '#000000', strokeWidth: 5 });
            canvas.add(obj);
        } else { console.log("addLine() - variable 'canvas' is null or undefined."); }
    }
}

function setCircleState() {
    if (editState == BRUSH) {
        if (fillColor) {
            fillColor.disabled = false;
            fillColor.value = '#FFFFFF';
        }
        if (strokeColor) {
            strokeColor.value = '#000000';
            strokeWidth.value = 3;
        }
    }
    editState = CIRCLE;
    if (canvas) {
        canvas.isDrawingMode = false;
        canvas.discardActiveObject();
    } else { console.log("setCircleState() - variable 'canvas' is null or undefined."); }

    selected = false; deselected = false;
}
function addCircle() {
    if (deselected) { deselected = false; selected = false; }
    else if (!selected) {
        if (canvas) {
            var point = canvas.getPointer(event.e), x = point.x, y = point.y;
            var obj = new fabric.Circle({ radius: 30, fill: '#FFFFFF', stroke:'#000000', left: x - 20, top: y - 20 });
            canvas.add(obj);
        } else { console.log("addCircle() - variable 'canvas' is null or undefined."); }
    }
}

function setRectangleState() {
    if (editState == BRUSH) {
        if (fillColor) {
            fillColor.disabled = false;
            fillColor.value = '#FFFFFF';
        }
        if (strokeColor) {
            strokeColor.value = '#000000';
            strokeWidth.value = 3;
        }
    }
    editState = RECTANGLE;
    if (canvas) {
        canvas.isDrawingMode = false;
        canvas.discardActiveObject();
    } else { console.log("setRectangleState() - variable 'canvas' is null or undefined."); }

    selected = false; deselected = false;
}
function addRectangle() {
    if (deselected) { deselected = false; selected = false; }
    else if (!selected) {
        if (canvas) {
            var point = canvas.getPointer(event.e), x = point.x, y = point.y;
            var obj = new fabric.Rect({ width: 100, height: 60, fill: '#FFFFFF', stroke: '#000000'});
            obj.set('left', x - (obj.width / 2));
            obj.set('top', y - (obj.height / 2));
            canvas.add(obj);
        } else { console.log("addRectangle() - variable 'canvas' is null or undefined."); }
    }
}

function setBrushState() {
    editState = BRUSH;
    if (canvas) {
        canvas.isDrawingMode = true;
        canvas.discardActiveObject();
    } else { console.log("setBrushState() - variable 'canvas' is null or undefined."); }

    if (fillColor) { fillColor.disabled = true; }
    else { console.log("setBrushState() - variable 'fillColor' is null or undefined."); }

    if (strokeColor) {
        strokeColor.value = canvas.freeDrawingBrush.color;
        strokeWidth.value = canvas.freeDrawingBrush.width;
    } else { console.log("setBrushState() - variable 'strokeColor' is null or undefined."); }

    selected = false; deselected = false;
}

function setTextState() {
    if (editState == BRUSH) {
        if (fillColor) {
            fillColor.disabled = false;
            fillColor.value = '#FFFFFF';
        }
        if (strokeColor) {
            strokeColor.value = '#000000';
            strokeWidth.value = 1;
        }
    }
    editState = TEXT;
    if (canvas) {
        canvas.isDrawingMode = false;
        canvas.discardActiveObject();
    }

    selected = false; deselected = false;
}
function addText() {
    if (deselected) { deselected = false; selected = false; }
    else if (!selected) {
        if (canvas) {
            var point = canvas.getPointer(event.e), x = point.x, y = point.y;
            var obj = new fabric.IText("Lorem ipsum");
            obj.left = x - (obj.width / 2); obj.top = y - (obj.height / 2);
            obj.set({
                fill		: '#000000',
                stroke		: '#000000',
                strokeWidth	: 1,
                fontSize	: 20
            });
            canvas.add(obj);
        } else { console.log("addText() - variable 'canvas' is null or undefined."); }
    }
}

function openImageDialog() {
    if (loadImageInput) { loadImageInput.click(); }
    else { console.log("openImageDialog() - variable 'loadImageInput' is null or undefined."); }
}

function addImageToCanvas() {
    /* If no files were chosen or user cancelled, do nothing. */
    var files = loadImageInput.files;
    if (files) {
        if (files.length <= 0) { return; }
        else {
            loading = true;
            var i = 0;
            for (i = 0; i < files.length; i++) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = new Image();
                    data.src = e.target.result;
                    data.onload = function () {
                        var image = new fabric.Image(data);

                        if (image.getScaledWidth() > canvas.getWidth()){
                            image.scale(canvas.getWidth() / image.getScaledWidth());
                        } else if (image.getScaledHeight() > canvas.getHeight()) {
                            image.scale(canvas.getHeight() / image.getScaledHeight());
                        }
                        if (canvas) {
                            canvas.centerObject(image);
                            canvas.add(image);
                            canvas.renderAll();
                        }
                    }
                };
                reader.readAsDataURL(files[i]);
            }
            loading = false;
        }
    } else { console.log("addImageToCanvas() - variable 'canvas' is null or undefined."); }
}

const stateFunctions = {
    LINE: addLine,
    CIRCLE: addCircle,
    RECTANGLE: addRectangle,
    TEXT: addText
}

function handleMouseDown(e) {
    startX = e.pointer.x; startY = e.pointer.y;
    if (selected) {
        if (canvas) {
            var current = canvas.getActiveObjects();
            if (current != null && current.length > 0) {
                offsetX = startX - current[0].left; offsetY = startY - current[0].top;
            }
        } else { console.log("handleMouseDown() - variable 'canvas' is null or undefined."); }
    } else if (editState != NONE && editState != BRUSH) { stateFunctions[editState](); }
}

function removeObject() {
    /* Check if editor can safely delete a selected object. */
    if (!loading && !saving) {
        if (canvas) {
            var current = canvas.getActiveObjects();
            if (current != null && current.length > 0) {
                var i = 0;
                for (i = 0; i < current.length; i++) {
                    canvas.remove(current[i]);
                }
                canvas.discardActiveObject();
            }
        } else { console.log("removeObject() - variable 'canvas' is undefined."); }
    }
}

var forwardButton, frontButton, backwardButton, backButton,
    fillColor, strokeColor, strokeWidth,
    setFont, setSize;

function bringForward() {
    if (current) {
        var current = canvas.getActiveObjects();
        // console.log(current);
        if (current.length > 0) {
            var i = 0;
            for (i = 0; i < current.length; i++) {
                canvas.bringForward(current[i]);
            }
            canvas.renderAll();
        }
    } else { console.log("bringForward() - variable 'canvas' is null or undefined."); }
}

function bringToFront() {
    if (canvas) {
        var current = canvas.getActiveObjects();
        if (current.length > 0) {
            var i = 0;
            for (i = 0; i < current.length; i++) {
                canvas.bringToFront(current[i]);
            }
            canvas.renderAll();
        }
    } else { console.log("bringToFront() - variable 'canvas' is null or undefined."); }
}

function sendBackwards() {
    if (canvas) {
        var current = canvas.getActiveObjects();
        // console.log(current);
        if (current.length > 0) {
            var i = 0;
            for (i = 0; i < current.length; i++) {
                canvas.sendBackwards(current[i]);
            }
            canvas.renderAll();
        }
    } else { console.log("sendBackwards() - variable 'canvas' is null or undefined."); }
}

function sendToBack() {
    if (canvas) {
        var current = canvas.getActiveObjects();
        // console.log(current);
        if (current.length > 0) {
            var i = 0;
            for (i = 0; i < current.length; i++) {
                canvas.sendToBack(current[i]);
            }
            canvas.renderAll();
        }
    } else { console.log("sendToBack() - variable 'canvas' is null or undefined."); }
}

function setFillColor(e) {
    if (canvas) {
        var current = canvas.getActiveObjects();
        if (current.length > 0) {
            var i = 0;
            for (i = 0; i < current.length; i++) {
                current[i].set({ fill: e.target.value });
            }
            canvas.renderAll();
        }
    } else { console.log("setFillColor() - variable 'canvas' is null or undefined."); }
}

function setStrokeColor(e) {
    if (editState == BRUSH) {
        if (canvas) { canvas.freeDrawingBrush.color = e.target.value; }
        else { console.log("setStrokeColor() - variable 'canvas' is null or undefined."); }
    } else {
        if (canvas) {
            var current = canvas.getActiveObjects();
            if (current.length > 0) {
                var i = 0;
                for (i = 0; i < current.length; i++) {
                    current[i].set({ stroke: e.target.value });
                }
                canvas.renderAll();
            }
        } else { console.log("setStrokeColor() - variable 'canvas' is null or undefined."); }
    }
}

function setStrokeWidth(e) {
    if (editState == BRUSH) {
        if (canvas) { canvas.freeDrawingBrush.width = e.target.value; }
        else { console.log("setStrokeWidth() - variable 'canvas' is null or undefined."); }
    } else {
        if (canvas) {
            var current = canvas.getActiveObjects();
            if (current.length > 0) {
                console.log(current);
                var i = 0;
                for (i = 0; i < current.length; i++) {
                    current[i].set({ strokeWidth: parseInt(e.target.value) });
                    current[i].setCoords();
                }
                canvas.renderAll();
            }
        } else { console.log("setStrokeWidth() - variable 'canvas' is null or undefined."); }
    }
}

function setTextFont(e) {
    if (canvas) {
        var current = canvas.getActiveObjects();
        if (current.length > 0) {
            console.log(current);
            var i = 0;
            for (i = 0; i < current.length; i++) {
                if (current[i] instanceof fabric.IText) {
                    console.log("setting font!");
                    current[i].set("fontFamily", e.target.value);
                }
            }
            canvas.renderAll();
        }
    } else { console.log("setTextFont() - variable 'canvas' is null or undefined."); }
}

function setTextSize(e) {
    if (canvas) {
        var current = canvas.getActiveObjects();
        if (current.length > 0) {
            console.log(current);
            var i = 0;
            for (i = 0; i < current.length; i++) {
                if (current[i] instanceof fabric.IText) {
                    console.log("setting size!");
                    current[i].set("fontSize", e.target.value);
                }
            }
            canvas.renderAll();
        }
    } else { console.log("setTextSize() - variable 'canvas' is null or undefined."); }
}

function saveState() {
    if (!undoing) {
        var state = canvas.toJSON();
        if (stateIndex < states.length - 1) {
            states.splice(stateIndex + 1, states.length - 1 - stateIndex);
        }
        states.push(state);
        stateIndex++;
        // console.log("saveState");
        // console.log(states);
        // console.log(stateIndex);
    }
}

function undoAction() {
    /* Check if editor has an action it can safely undo. */
    if (!loading && !saving && stateIndex > 0) {
        stateIndex--;
        var lastState = states[stateIndex];

        undoing = true;

        canvas.loadFromJSON(lastState, function() {
            canvas.renderAll();
            undoing = false;
        });

        console.log("undo");
        console.log(states);
        console.log(stateIndex);
    }
}

function redoAction() {
    if (!loading && !saving && stateIndex < states.length - 1) {
        stateIndex++;
        var nextState = states[stateIndex];

        undoing = true;

        canvas.loadFromJSON(nextState, function() {
            canvas.renderAll();
            undoing = false;
        });

        console.log("redo");
        console.log(states);
        console.log(stateIndex);
    }
}

function newFile() {
    if (states.length > 0) {
        /* Alert user that they have unsaved changes. */
        if (!window.confirm(UNSAVED)) { return; }
    }
    /* Reset editor by clearing all canvas elements. */
    states = [];
    selected = false; deselected = false;
    canvas.discardActiveObject();
    undoing = false;
    editState = NONE;
    loading = false; saving = false;
    canvas.clear();
}

function loadFile() {
    if (states.length > 0) {
        /* Alert user that they have unsaved changes. */
        if (!window.confirm(UNSAVED)) { return; }
    }
    /* If no files were chosen or user cancelled, do nothing. */
    var files = loadFileInput.files;
    //console.log(files);
    if (files.length <= 0) { return; }
    else {
        var file = files[0];
        //console.log(file);
        loading = true;
        var reader = new FileReader();
        reader.onload = function(e) {
            var contents = e.target.result,
                json = JSON.parse(contents);
            //console.log(json);
            canvas.loadFromJSON(json);
            //console.log(canvas.getObjects());
            loading = false;
        };
        reader.readAsText(file);
    }
}

function saveFile() {
    /* If no changes were made, do nothing. */
    if (states.length > 1) {
        /* Create JSON object from Fabric.js Canvas. */
        var a = document.createElement("a");
        var json = canvas.toJSON();
        content = JSON.stringify(json);

        // var file = new Blob([content], {type: "application/json"});
        // a.href = URL.createObjectURL(file);
        // a.download = "drawing";
        //a.click();

        var pageObj = {};
        Object.assign(pageObj, pageTemplate);
        pageObj.id = Math.floor(Math.random() * 10000);
        pageObj.image = content;
        pageObj.pageNumber = -1;

        pageId = document.getElementById("pgname");
        pageNumber = document.getElementById("pgnumber");
        targetComic = document.getElementById("comicname");
        //console.log(targetComic.value);
        if(pageId.value == "" || pageNumber.value == "" || targetComic.value == ""){
            document.getElementById("errorSaveMessage").innerHTML = "Please fill out all fields."
        }
        else{
            pageId.value = pageId.value + "-" + Math.floor(Math.random() * 999999).toString();
            checkForDuplicatePageName(pageId.value,function(){
                //console.log("attempt query");
                //console.log(currentUser);
                $.get('/byUser/userName/' + currentUser, function(data) {
                    userResultsHolder = data;
                    //console.log("USER RESULTS HOLDER:");
                    //console.log(userResultsHolder);
                    //console.log(userResultsHolder.userID);
                    //loading = false;
                    //putinPage(pageId, pageNumber, targetComic, userResultsHolder);
                    console.log("WAS THERE AN ALREADY EXISTING NAME?");
                    console.log(lastDuplicateCheck===true);
                    if(!(lastDuplicateCheck===true)){
                        $.post('/Page/AddParamPage/' + pageId.value + '/' + pageNumber.value + '/'
                            + targetComic.value + '/' + userResultsHolder.userID, pageObj, function() {
                            console.log("Page has made a post call!");
                            document.getElementById("errorSaveMessage").innerText = "Page saved to the desired comic!"
                        }).fail(function() {
                            console.log("Could not upload new Page.");
                        });

                    }
                    else{
                        $.post('/Page/UpdateParamPage/' + pageId.value + '/' + pageNumber.value + '/'
                            + targetComic.value + '/' + userResultsHolder.userID, pageObj, function() {
                            console.log("Page has made a post call!");
                            document.getElementById("errorSaveMessage").innerText = pageId.value + "was updated!"
                        }).fail(function() {
                            console.log("Could not upload new Page.");
                        });
                    }
                }).fail(function() {
                    console.log("Could not get user id to add page to.");
                    //loading = false;
                });

                //window.alert("Page saved to the desired comic!");
            });
        }
    }
}

var myPageChooser;
function updateLoadedPage(){
    var pageToUpdate = document.getElementById("lastloaded").innerText;
    $.get("/Page/" + pageToUpdate, function(data) {
        myPageChooser = data;


        var a = document.createElement("a");
        var json = canvas.toJSON();
        content = JSON.stringify(json);

        // var file = new Blob([content], {type: "application/json"});
        // a.href = URL.createObjectURL(file);
        // a.download = "drawing";
        //a.click();

        var pageObj = {};
        Object.assign(pageObj, pageTemplate);
        pageObj.id = pageToUpdate;
        pageObj.image = content;
        pageObj.pageNumber = myPageChooser.pageNumber;


        $.post('/Page/UpdateParamPage/' + pageToUpdate + '/' + myPageChooser.pageNumber + '/'
            + myPageChooser.comicId + '/' + myPageChooser.userId, pageObj, function() {
            console.log("Page has made a post call!");
            document.getElementById("errorSaveMessageU").innerText = pageToUpdate + "was updated!"
        }).fail(function() {
            console.log("Could not upload new Page.");
        });
    });

}


var pageAllResultsHolder;
var lastDuplicateCheck=false;
function checkForDuplicatePageName(pageName, callback){
    lastDuplicateCheck=false;
    $.get('/Page/getAllPageNamesThisUserHas', function(data) {
        pageAllResultsHolder = data;
        //console.log(pageAllResultsHolder);
        //console.log(typeof(pageAllResultsHolder));
        var i = 0

        for(i=0; i<pageAllResultsHolder.length; i++){
            console.log(pageAllResultsHolder[i]);
            console.log(pageAllResultsHolder[i] === pageName);
            if(pageAllResultsHolder[i] === pageName){
                console.log("LDC CHANGE HIT");
                lastDuplicateCheck=true;
            }
        }
        console.log("checkForDuplicatePageName has finished, proceeding to callback");
        callback();
    }).fail(function() {
        console.log("Could not find a pages array tied to the user account");
    });



}

function downloadFile() {
    /* If no changes were made, do nothing. */
    if (states.length > 0) {
        var a = document.createElement("a");
        // console.log(canvas.getObjects());
        var json = canvas.toJSON();
        content = JSON.stringify(json);

        var file = new Blob([content], {type: "application/json"});
        a.href = URL.createObjectURL(file);
        a.download = "drawing";
        a.click();
    }
}

function updateSelection(e) {
    selected = true;
    editState = NONE;

    var current = canvas.getActiveObjects();
    if (current.length > 0) {
        fillColor.value = (current[0].fill) ? current[0].fill : "#FFFFFF";
        strokeColor.value = (current[0].stroke) ? current[0].stroke : '#000000';
        strokeWidth.value = (current[0].strokeWidth) ? current[0].strokeWidth : 3;
        for (var i = 0; i < current.length; i++) {
            if (current[i] instanceof fabric.IText) {
                console.log(current[i]);
                setFont.disabled = false;
                setFont.value = (current[i].fontFamily) ? current[i].fontFamily : "Times New Roman";
                setSize.disabled = false;
                setSize.value = (current[i].fontSize) ? current[i].fontSize : "20";
                break;
            }
        }
    }
}
function clearSelection(e) {
    selected = false;
    editState = NONE;

    fillColor.value = "#FFFFFF";
    strokeColor.value = "#000000";
    strokeWidth.value = 3;

    setFont.disabled = true;
    setSize.disabled = true;
}

/* Initialize drawing tool by adding all control functions. */
window.onload = function() {
    /* Retrieve drawing tool control elements from HTML page. */
    undo		= document.getElementById("undo");
    redo		= document.getElementById("redo");
    del			= document.getElementById("del");
    line		= document.getElementById("line");
    circ		= document.getElementById("circ");
    rect		= document.getElementById("rect");
    brush		= document.getElementById("brush");
    txt			= document.getElementById("txt");
    img         = document.getElementById("img");
    loadImageInput  = document.getElementById("loadImageInput");

    /* Set states for main tools. */
    undo.onclick	= undoAction;
    redo.onclick	= redoAction;
    del.onclick		= removeObject;
    line.onclick	= setLineState;
    circ.onclick	= setCircleState;
    rect.onclick	= setRectangleState;
    brush.onclick	= setBrushState;
    txt.onclick		= setTextState;
    img.onclick     = openImageDialog;
    loadImageInput.addEventListener("change", addImageToCanvas);

    /* Set states for object editing tools/buttons. */
    backButton		= document.getElementById("backButton");
    backwardButton 	= document.getElementById("backwardButton");
    frontButton		= document.getElementById("frontButton");
    forwardButton	= document.getElementById("forwardButton");

    backButton.onclick 		= sendToBack;
    backwardButton.onclick	= sendBackwards;
    frontButton.onclick 	= bringToFront;
    forwardButton.onclick 	= bringForward;

    fillColor		= document.getElementById("fillColor");
    strokeColor		= document.getElementById("strokeColor");
    strokeWidth		= document.getElementById("strokeWidth");

    fillColor.addEventListener("change", setFillColor);
    strokeColor.addEventListener("change", setStrokeColor);
    strokeWidth.addEventListener("change", setStrokeWidth);

    setFont			= document.getElementById("setFont");
    setSize			= document.getElementById("setSize");

    setFont.addEventListener("change", setTextFont);
    setSize.addEventListener("change", setTextSize);

    /* Retrive file I/O button elements from HTML page. */
    newButton		= document.getElementById("newFile");
    loadButton		= document.getElementById("loadFile");
    loadFileInput	= document.getElementById("loadFileInput");
    saveButton		= document.getElementById("saveFile");
    downloadButton	= document.getElementById("downloadFile");

    /* Set states for file I/O controls. */
    newButton.onclick 	= newFile;
    saveButton.onclick	= saveFile;
    downloadButton.onclick	= downloadFile;
    loadFileInput.addEventListener('change', loadFile);

    /* Set up Fabric canvas in Drawing Tool page. */
    canvas = new fabric.Canvas('canvas');
    canvas.preserveObjectStacking = true;
    canvas.selectionColor = 'rgba(255,255,255,0.3)';
    canvas.selectionBorderColor = 'red';
    canvas.selectionLineWidth = 5;

    /* Set width of free drawing brush. */
    canvas.freeDrawingBrush.color = '#000000';
    canvas.freeDrawingBrush.width = 5;

    /* Add listeners to Fabric canvas. */
    canvas.on("selection:created", updateSelection);
    canvas.on("selection:updated", updateSelection);
    canvas.on("selection:cleared", clearSelection);
    canvas.on("mouse:down", handleMouseDown);
    canvas.on("object:added", saveState);
    canvas.on("object:modified", saveState);
    canvas.on("object:removed", saveState);

    states.push(canvas.toJSON());

    currentUser = document.getElementById("username").innerHTML;



    //in case of image size issues we get directly from the database by id, rather
    //than pass the image string itself
    pageID = sessionStorage.getItem("requestedPageId");
        console.log("=============================PAGE ID=============================================");
        console.log(pageID);
        loadImageFromPageId(pageID);
        console.log("=============================PAGE ID=============================================");

};


function loadImageFromPageId(name){
    if(name == null | name == "null"){
        console.log("NAME IS NULL");

    }
    else{
        $.get('/Page/' + name, function(data) {
            console.log("Page has made page get call");
            holdPageInfo = data;

            json = JSON.parse(holdPageInfo.image);
            canvas.loadFromJSON(json);

            document.getElementById("lastloadedStatememt").innerText="You loaded:";
            document.getElementById("lastloaded").innerText=holdPageInfo.id;
            document.getElementById("theUpdatingButton").disabled="";
        }).fail(function() {
            console.log("Could not get page's data.");
        });
    }


}


window.addEventListener('beforeunload', (event) => {
    if (states.length > 1) {
    // Cancel the event as stated by the standard.
    event.preventDefault();
    // Chrome requires returnValue to be set.
    event.returnValue = 'Are you sure?';
}
});

window.onunload= function() {
    //resets the page image to be loaded in to nothing everytime we leave the page
    sessionStorage.setItem("requestedPageId", null);
};