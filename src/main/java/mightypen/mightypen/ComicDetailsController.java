package mightypen.mightypen;

import mightypen.mightypen.Repositories.ComicRepository;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import mightypen.mightypen.Data.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Customized functions pertaining to Comic and Series Management.
 * @author  Peter Ly
 * @version 1.0
 */
@RestController
@RequestMapping("/")
public class ComicDetailsController {

    UserRepository userRepo;
    SeriesRepository seriesRepo;
    ComicRepository comicRepo;

    public ComicDetailsController(SeriesRepository seriesRepo, UserRepository userRepo, ComicRepository comicRepo)
    {
        this.seriesRepo = seriesRepo;
        this.userRepo = userRepo;
        this.comicRepo = comicRepo;
    }

    @PostMapping(path = "/AddComicWithPiD")
    public ModelAndView AddComicWithPiD(@ModelAttribute Comics comic) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        try
        {
            mightypen.mightypen.Data.User targetUser = userRepo.findByUserName(username);
            //may need to change this to creation of a new comic.
            comic.setUserId(targetUser.getUserID());
            comic.setDescription("insert description here");
            comic.setSeriesId("");
            this.comicRepo.save(comic);

        }
        catch(Exception e)
        {
            System.out.println("Comic was not added!");
        }
        ModelAndView mv = new ModelAndView("manageComic.html");
        return mv;

    }

    @PostMapping(path = "/AddSeriesWithPiD")
    public ModelAndView AddSeriesWithPiD(@ModelAttribute Series series) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        try
        {
            mightypen.mightypen.Data.User targetUser = userRepo.findByUserName(username);
            //note we may want to replace this with consrtuctor
            series.setUserId(targetUser.getUserID());
            series.setDescription("insert description here");
            series.setComicID(new ArrayList<String>());
            series.setTag(new ArrayList<String>());
            series.setPublished(false);
            this.seriesRepo.save(series);

        }
        catch(Exception e)
        {
            System.out.println("Comic was not added!");
        }
        ModelAndView mv = new ModelAndView("manageSeries.html");
        return mv;

    }



    @PostMapping(value=
            "/comicUpdateDescription/{comicName}/{newDescription}"
    )
    public ModelAndView updateComicDescription(
                                               @PathVariable("comicName") String comicName,
                                               @PathVariable("newDescription") String newDescription) {

            //System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());

            try {
                Comics comic = comicRepo.findByTitle(comicName);
                comic.setDescription(newDescription);
                this.comicRepo.save(comic);

            } catch (Exception e) {
                System.out.println("Comic was not updated!");
            }
            //doesn't work for some reason. had to force a reload in JS
            ModelAndView mv = new ModelAndView("manageComic.html");
            return mv;

    }


    @PostMapping(value=
            "/updateComicDescriptionFlexible/{comicName}/{newDescription}/{newName}"
    )
    public ModelAndView updateComicDescriptionFlexible(
            @PathVariable("comicName") String comicName,
            @PathVariable("newDescription") String newDescription,
            @PathVariable("newName") String newName) {

        //System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        try {
            Comics comic = comicRepo.findByTitle(comicName);
            if(!newDescription.equals("")){
                comic.setDescription(newDescription);
            }
            if(!newName.equals("")){
                comic.setTitle(newName);
            }
            this.comicRepo.save(comic);

        } catch (Exception e) {
            System.out.println("Comic was not updated!");
        }
        //doesn't work for some reason. had to force a reload in JS
        ModelAndView mv = new ModelAndView("manageComic.html");
        return mv;

    }

    @PostMapping(value=
            "/seriesUpdateDescription/{seriesName}/{newDescription}"
    )
    public ModelAndView updateSeriesDescription(
            @PathVariable("seriesName") String seriesName,
            @PathVariable("newDescription") String newDescription) {

        //System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        try {
            Series series = seriesRepo.findByTitle(seriesName);
            series.setDescription(newDescription);
            this.seriesRepo.save(series);

        } catch (Exception e) {
            System.out.println("Series was not updated!");
        }
        //doesn't work for some reason. had to force a reload in JS
        ModelAndView mv = new ModelAndView("manageSeries.html");
        return mv;

    }


    @PostMapping(value=
            "/updateSeriesDescriptionFlexible/{seriesName}/{newDescription}/{newName}"
    )
    public ModelAndView updateSeriesDescriptionFlexible(
            @PathVariable("seriesName") String seriesName,
            @PathVariable("newDescription") String newDescription,
            @PathVariable("newName") String newName) {

        //System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);


        Series s = null;
        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Series>  userSeries = seriesRepo.findByUserId(user.getUserID());

            for (Series stone : userSeries) {
                if (stone.getTitle().equals(seriesName)){
                    if(stone.getUserId().equals(user.getUserID())){
                        s = stone;
                    }
                }
            }


            //Series series = seriesRepo.findByTitle(seriesName);
            if(!newDescription.equals("")){
                s.setDescription(newDescription);
            }
            if(!newName.equals("")){
                s.setTitle(newName);
            }
            this.seriesRepo.save(s);
        }
        catch (Exception e)
        {
            System.out.println("Was not able to get this page.");
            System.out.println(e);
        }

        /*
        try {
            Series series = seriesRepo.findByTitle(seriesName);
            if(!newDescription.equals("")){
                series.setDescription(newDescription);
            }
            if(!newName.equals("")){
                series.setTitle(newName);
            }
            this.seriesRepo.save(series);

        } catch (Exception e) {
            System.out.println("Comic was not updated!");
        }
         */
        //doesn't work for some reason. had to force a reload in JS
        ModelAndView mv = new ModelAndView("manageComic.html");
        return mv;


    }



    //Get comics that are NOT parented to a series
    @GetMapping("/getUnparentedComics")
    public ArrayList<Comics> getFreeComics()
    {
        System.out.println("unparented comics");
        ArrayList<Comics> comicsListToKeep = new ArrayList<Comics>();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Comics> comicsListToPrune = comicRepo.findByUserId(user.getUserID());
            System.out.println(comicsListToPrune);
            for(Comics c : comicsListToPrune){
                System.out.println(c.getTitle());
                if (c.getSeriesId().equals("")){
                    comicsListToKeep.add(c);
                }
            }
        }
        catch(Exception e)
        {
            System.out.println("Comic search for ONLY free comics (not parented by series) was not successful.");
            System.out.println(e);
        }
        return comicsListToKeep;
    }

    /* Search for a Comic by title - meant to allow a user to search for individual Comics. */
    @GetMapping("/getComicByTitle/{title}")
    public ArrayList<Comics> searchComicByTitle(@PathVariable("title") String title) {
        System.out.println("Searching comics by title regex...");
        ArrayList<Comics> results = comicRepo.findByTitleRegex(title);
        return results;
    }


    /* Search for a Comic by title BUT REMOVE UNPUBLISHED entries */
    @GetMapping("/getComicByPublishedTitle/{title}")
    public ArrayList<Comics> getComicByPublishedTitle(@PathVariable("title") String title) {
        System.out.println("Searching comics by title regex...");
        ArrayList<Comics> results = comicRepo.findByTitleRegex(title);
        ArrayList<Comics> resultsTrue = new ArrayList<Comics>();
        for(Comics c : results){
            Series checker = seriesRepo.findBySeriesID(c.getSeriesId());
            if(checker != null && checker.getPublished() == true){
                resultsTrue.add(c);
            }
        }
        return resultsTrue;
    }

    /* Search for Comic by user's ID - meant for the 'search by author' functions in search.js. */
    @GetMapping("/getComicByUserID/{userID}")
    public ArrayList<Comics> searchComicByUserID(@PathVariable("userID") String userID) {
        System.out.println("Searching comics by user ID...");
        ArrayList<Comics> results = comicRepo.findByUserId(userID);
        return results;
    }
}
