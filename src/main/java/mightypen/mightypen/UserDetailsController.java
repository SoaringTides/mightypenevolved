package mightypen.mightypen;

//import mightypen.mightypen.Data.Series;
//import mightypen.mightypen.Data.User;
import mightypen.mightypen.Data.*;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Customized functions pertaining to User account editing.
 * @author  Peter Ly
 * @version 1.0
 */
@RestController
@RequestMapping("/")
public class UserDetailsController {
    @Autowired
    UserRepository userRepo;
    SeriesRepository seriesRepo;

    @PostMapping(path = "/deleteThisUser")
    public ModelAndView deleteThisUser() {
        //System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            this.userRepo.deleteById(user.getUserID());

            System.out.println("Attempting to wipe this session...");
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            SecurityContextHolder.clearContext();
            attr.getRequest().getSession(true).invalidate();

        }
        catch (Exception e)
        {
            System.out.println("User was not deleted!");
            System.out.println(e);
        }
        System.out.println("Delete This User Run Completed!");

        ModelAndView mv = new ModelAndView("index");
        return mv;
    }


    @PostMapping(path = "/UpdateThisUser")
    public ModelAndView updateCustom(@ModelAttribute User user) {
        //System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        try
        {
            mightypen.mightypen.Data.User targetUser = userRepo.findByUserName(username);
            userRepo.save(new User(targetUser.getUserID(),user.getUserName(), user.getPassword(), user.getFirstName(),
                    user.getLastName(),user.getEmailAddress()));
            /*UPDATING THE USERNAME IN SESSION


             */

            /*
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            System.out.println(attr);
            (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal().setUserName();
             */

            //SecurityContextHolder.getContext().getAuthentication().getName();

            Collection<SimpleGrantedAuthority> currentAuthorities =(Collection<SimpleGrantedAuthority>)SecurityContextHolder
                    .getContext().getAuthentication().getAuthorities();
            UsernamePasswordAuthenticationToken updateAuth = new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPassword(), currentAuthorities);
            SecurityContextHolder.getContext().setAuthentication(updateAuth);

            System.out.println("If this printed it means the session got updated too.");
        }
        catch (Exception e)
        {
            System.out.println("User was not updated!");
            System.out.println(e);
        }
        System.out.println("User Update Run Completed!");
        ModelAndView mv = new ModelAndView("/manageAccount.html");
        return mv;
    }

    @PostMapping(path = "/AddUserAndRedirect")
    public ModelAndView add(@ModelAttribute User user) {

        ModelAndView mv = new ModelAndView("/index.html");
        try
        {
            this.userRepo.save(user);
        }
        catch(Exception e)
        {
            System.out.println("User was not added!");
        }

        return mv;

    }

    @PostMapping(path = "/ObtainSession")
    public void getSessionInfo() {

        try
        {
            /*
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpSession session = attr.getRequest().getSession();
            System.out.println(attr.getRequest().getSession());
            System.out.println(RequestContextHolder.currentRequestAttributes().getSessionId());
            */

            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = "";

            if (principal instanceof UserDetails) {
                System.out.println("user details");
                username = ((UserDetails)principal).getUsername();

            } else {
                //System.out.println("user string");
                username = principal.toString();

            }

            System.out.println(username);
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);

            System.out.println(user.getUserID());
            System.out.println(user.getUserName());
            System.out.println(user.getFirstName());
            System.out.println(user.getLastName());
            System.out.println(user.getEmailAddress());

        }
        catch(Exception e)
        {
            System.out.println("Session not obtained");
        }
    }

    @PostMapping(path="/addSubscription/{userID}/{seriesID}")
    public void addSubscription(@PathVariable("userID") String userID, @PathVariable("seriesID") String seriesID) {
        try {
            mightypen.mightypen.Data.User user = userRepo.findByUserID(userID);
            user.addSubscriptions(seriesID);
            userRepo.save(user);
            System.out.println("Subscription has been added!");
        } catch (Exception e) {
            System.out.println("Could not add subscription!");
        }
    }

    @PostMapping(path="/removeSubscription/{userID}/{seriesID}")
    public void removeSubscription(@PathVariable("userID") String userID, @PathVariable("seriesID") String seriesID) {
        try {
            mightypen.mightypen.Data.User user = userRepo.findByUserID(userID);
            user.removeSubscriptions(seriesID);
            userRepo.save(user);
            System.out.println("Subscription has been removed!");
        } catch (Exception e) {
            System.out.println("Could not remove subscription!");
        }
    }
}
