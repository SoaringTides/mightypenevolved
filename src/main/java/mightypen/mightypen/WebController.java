package mightypen.mightypen;

import mightypen.mightypen.Data.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * Customized functions pertaining to various web views.
 * @author  Peter Ly
 * @version 1.0
 */
@RestController
@RequestMapping()
public class WebController
{
    public WebController(){};


    @GetMapping(path = "/login")
    public ModelAndView getLoginPage()
    {
        ModelAndView mv = new ModelAndView("index");
        System.out.println("Login");

        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        //System.out.println(auth);
        return mv;

    }

    @GetMapping(path = "/home")
    public ModelAndView getHomePage()
    {
        ModelAndView mv = new ModelAndView("home");

        System.out.println("Home");

        return mv;

    }

    @GetMapping(path = "/hello")
    public ModelAndView getHelloPage()
    {
        ModelAndView mv = new ModelAndView("hello");

        System.out.println("hello");

        return mv;

    }

    @GetMapping(path = "/")
    public ModelAndView getHome()
    {
        ModelAndView mv = new ModelAndView("search");
        System.out.println("default to search");

        return mv;

    }

    @GetMapping("/drawingtool")
    public ModelAndView drawingToolRedirect()
    {

        ModelAndView mv = new ModelAndView("drawingTool.html");
        return mv;
    }


}




