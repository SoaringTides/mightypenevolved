package mightypen.mightypen.Repositories;

import java.util.ArrayList;
import java.util.List;

import mightypen.mightypen.Data.Comics;
import mightypen.mightypen.Data.Pages;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PageRepository extends MongoRepository<Pages, String> {

    public Pages findByid(String pageId);
    public Pages findByPageNumber(String pageNumber);

    public Pages deleteByIdAndUserId(String id, String userId);


    /**
     * Searching for multiple pages.
     * @author  Peter Ly
     * @version 1.0
     */
    public ArrayList<Pages> findByUserId(String userId);

}