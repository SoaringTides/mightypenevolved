package mightypen.mightypen.Repositories;

import java.util.ArrayList;
import java.util.List;
import mightypen.mightypen.Data.Comics;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ComicRepository extends MongoRepository<Comics, String> {

    public Comics findByComicId(String comicId);
    public Comics findByTitle(String title);


    /**
     * Searching for multiple comics.
     * @author  Peter Ly
     * @version 1.0
     */
    public ArrayList<Comics> findByUserId(String userId);

    @Query(value = "{'title': {$regex : ?0, $options: 'i'}}")
    public ArrayList<Comics> findByTitleRegex(String regexString);
}
