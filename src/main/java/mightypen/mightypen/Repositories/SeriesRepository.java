package mightypen.mightypen.Repositories;


import java.util.ArrayList;
import java.util.List;

import mightypen.mightypen.Data.Comics;
import mightypen.mightypen.Data.Series;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface SeriesRepository extends MongoRepository<Series, String> {

    public Series findBySeriesID(String seriesID);
    public Series findByTitle(String title);

    /**
     * Regex Searching for deesired series.
     * @author  Peter Ly
     * @version 1.0
     */
    //@Query("{$or : [{'name': { $regex: ?0, $options:'i' }}, {'description': { $regex: ?0, $options:'i' }}]}")
    //INCORRECT  - @Query("{'title': { $regex: : /.*titleInput.*/, $options: 'i } })")
    @Query(value = "{'title': {$regex : ?0, $options: 'i'}}")
    public List<Series> findByTitleRegex(String regexString);

    @Query(value = "{'userId': {$regex : ?0, $options: 'i'}}")
    public List<Series> findByAuthorRegex(String regexString);

    public ArrayList<Series> findByUserId(String userId);

}