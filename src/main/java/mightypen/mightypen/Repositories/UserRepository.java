package mightypen.mightypen.Repositories;

import java.util.List;
import mightypen.mightypen.Data.User;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    public User findByUserID(String userId);
    public User findByUserName(String userName);

}