package mightypen.mightypen;

import mightypen.mightypen.Repositories.ComicRepository;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import mightypen.mightypen.Data.*;

import java.util.ArrayList;
/**
 * Rest controller to grab Series by UserId
 * @author  Peter Ly
 * @version 1.0
 */
@RestController
@RequestMapping("/Series")
public class SeriesRestController {
    UserRepository userRepo;
    SeriesRepository seriesRepo;
    ComicRepository comicRepo;

    public SeriesRestController(SeriesRepository seriesRepo, UserRepository userRepo, ComicRepository comicRepo)
    {
        this.seriesRepo = seriesRepo;
        this.userRepo = userRepo;
        this.comicRepo = comicRepo;
    }

    //Get comic by author's userid
    //these two methods were ambiguous, resolved by adding distinct identifiers
    @GetMapping("/seriesUserId/{userId}")
    public ArrayList<Series> getSeriesByUserId(@PathVariable("userId") String userId)
    {
        return seriesRepo.findByUserId(userId);
    }

    //Delete Comic by Title AND UserId (this is to prevent same-named comics from being deleted!)
    //we use spring security to get the user's username
    //so that we can just do a database call here to get ID.
    //then we'll pull all the user's comics, determine which one matches
    //the title entered, and delete only that one through its id
    @PostMapping(path = "/deleteSeriesByUserIdScan/{title}")
    public void deleteSeriesByUserIdScan(@PathVariable("title") String title) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Series> seriesarray = seriesRepo.findByUserId(user.getUserID());
            Series target = null;
            System.out.println("Title is:");
            System.out.println(title);
            for (Series s : seriesarray)
            {
                System.out.println(s.getTitle());
                if (s.getTitle().equals(title)){
                    target = s;
                }
            }
            if(target != null){

                ArrayList<Comics> comicarray = comicRepo.findByUserId(user.getUserID());

                for (Comics c : comicarray)
                {
                    if (c.getSeriesId().equals(target.getSeriesID())){
                        c.setSeriesId("");
                        comicRepo.save(c);
                    }
                }

                seriesRepo.deleteById(target.getSeriesID());
                //note that this only deletes the last named comic assuming there's multiples
                // with same comic and userid maker
                // (which there shouldn't be)
            }
            else{
                System.out.println("comic not found");
            }

        }
        catch (Exception e)
        {
            System.out.println("Comic  was not deleted!");
            System.out.println(e);
        }
    }


    //insert comic id into series
    @PostMapping(path = "/insertComicIntoSeries/{seriesTitle}/{comicTitleToAdd}")
    public void insertComicIntoSeries(@PathVariable("seriesTitle") String seriesTitle,
            @PathVariable("comicTitleToAdd") String comicTitleToAdd) {
        Series s = seriesRepo.findByTitle(seriesTitle);
        Comics c= comicRepo.findByTitle(comicTitleToAdd);

        //System.out.println(c.toString());
        //System.out.println(c.getComicId().toString());

        String newId = c.getComicId().toString();
        c.setSeriesId(newId);
        s.insertComicId(newId);
        c.setSeriesId(s.getSeriesID());
        seriesRepo.save(s);
        comicRepo.save(c);
        System.out.println("Series-Comic link completed.");
    }
}
