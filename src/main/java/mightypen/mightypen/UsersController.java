package mightypen.mightypen;

import mightypen.mightypen.Data.Series;
import mightypen.mightypen.Data.User;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@RestController
@RequestMapping("/byUser")
public class UsersController {


    UserRepository userRepo;
    SeriesRepository seriesRepo;

    public UsersController(UserRepository Ur, SeriesRepository Sr)
    {
        this.userRepo = Ur;
        this.seriesRepo = Sr;
    }


    //Returns a user by their Id
    @GetMapping("/userId/{userId}")
    public User getUserById(@PathVariable("userId") String userId)
    {

        System.out.println("User id from url is: " + userId);
        User user = userRepo.findByUserID(userId);

        return user;
    }

    //Returns a user by their username
    @GetMapping("/userName/{userName}")
    public User getUserByUserName(@PathVariable("userName") String userName)
    {
        User user = userRepo.findByUserName(userName);

        return user;
    }

    //Returns list of all series from a single user
    @GetMapping("/series/{userId}")
    public ArrayList<Series> getUserSeries(@PathVariable("userId") String userId)
    {

        User user = userRepo.findByUserID(userId);

        //Gets list of all series Ids
        ArrayList<String> seriesIdList = user.getSeriesIds();
        //ArrayList which holds all series in a collection
        ArrayList<Series> seriesList = new ArrayList<Series>();

        for(int i = 0; i < seriesIdList.size();i++)
        {
            Series s = seriesRepo.findBySeriesID(seriesIdList.get(i));
            seriesList.add(s);

        }

        return seriesList;
    }

    @PutMapping(path = "/UpdateUser")
    public void update(@ModelAttribute User user) {
        String id = user.getUserID();

        this.userRepo.insert(user);

    }

    @PostMapping(path = "/AddUser")
    public ModelAndView add(@ModelAttribute User user) {

        ModelAndView mv = new ModelAndView("/index.html");
        try
        {
            this.userRepo.save(user);


        }
        catch(Exception e)
        {
           System.out.println("User was not added!");
        }

        return mv;

    }

    @PostMapping(path = "/deleteUser")
    public void delete(@ModelAttribute User user) {

        try
        {
            this.userRepo.deleteById(user.getUserID());
        }
        catch (Exception e)
        {
            System.out.println("User was not deleted!");
        }
    }


}
