package mightypen.mightypen.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document(collection = "Series")
public class Series {

    @Id
    private String seriesID;
    private String title;
    private ArrayList<String> comicID;
    private String description;
    private Boolean inProgress;
    private ArrayList<String> tag;
    private String userId;
    private Boolean published;

    public Series(String userId, String seriesId, String title, String description, boolean inProgress) {
        this.userId = userId;
        this.seriesID = seriesId;
        this.title = title;
        this.description = description;
        this.inProgress = inProgress;
        this.tag = new ArrayList<>();
        this.comicID = new ArrayList<>();
        this.published = !inProgress;
    }

    public Series() { }

    public Series(String seriesID, String title, String description) {
        this.seriesID = seriesID;
        this.title = title;
        this.description = description;
        this.published = false;
    }

    public Series(String seriesID, String userId, String title, String description) {
        this.seriesID = seriesID;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.published = false;
    }
    public Series( String title) {
        this.title = title;
    }

    public String getSeriesID() {
        return seriesID;
    }

    public void setSeriesID(String seriesID) {
        this.seriesID = seriesID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getComicID() {
        return comicID;
    }

    public void setComicID(ArrayList<String> comicID) {
        this.comicID = comicID;
    }

    public void addComicID(String comicID) { this.comicID.add(comicID); }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getInProgress() {
        return inProgress;
    }

    public void setInProgress(Boolean inProgress) {
        this.inProgress = inProgress;
    }

    public ArrayList<String> getTag() {
        return tag;
    }

    public void setTag(ArrayList<String> tag) {
        this.tag = tag;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean getPublished() {
        if (this.published == null) {
            this.published = false;
        }
        return this.published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public void insertComicId(String cid){ this.comicID.add(cid);}
    public void deleteComicId (String cid) { this.comicID.remove(cid);}
}

/**
 * Various adjustments to constructors were made by Peter Ly
 */
