package mightypen.mightypen.Data;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Document(collection = "Comics")
public class Comics {

    @Id
    private String comicId;
    private String title;
    private String userId;
    private boolean inProgress;
    private ArrayList<String> tag;
    private String description;
    private ArrayList<String> pageId = new ArrayList<String>();
    private String startPage;
    private String numberofPages;
    private boolean published;
    private String seriesId;

    public Comics(String userId, String comicId, String title, String description, boolean inProgress) {
        this.userId = userId;
        this.comicId = comicId;
        this.title = title;
        this.description = description;
        this.inProgress = inProgress;
        this.published = !inProgress;
        this.tag = new ArrayList<>();
        this.pageId = new ArrayList<>();
        this.startPage = "";
        this.numberofPages = "";
        this.seriesId="";
    }
    public Comics() { }

    public Comics(String title) {
        this.title = title;
        this.startPage = "";
        this.numberofPages = "";
        this.seriesId="";
    }

    public Comics(String title, String userId) {
        this.title = title;
        this.userId = userId;
        this.startPage = "";
        this.numberofPages = "";
        this.seriesId="";
    }

    public Comics(String title, String userId, String description) {
        this.title = title;
        this.userId = userId;
        this.description = description;
        this.startPage = "";
        this.numberofPages = "";
        this.seriesId="";
    }

    public String getSeriesId() {
        return seriesId;
    }
    public void setSeriesId(String seriesId) {
        this.seriesId = seriesId;
    }

    public String getComicId() {
        return comicId;
    }

    public void setComicId(String comicId) {
        this.comicId = comicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean getInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }

    public ArrayList<String> getTag() {
        return tag;
    }

    public void setTag(ArrayList<String> tag) {
        this.tag = tag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getPageId() {
        return pageId;
    }

    public void setPageId(ArrayList<String> pageId) {
        this.pageId = pageId;
    }

    public void addPageID(String pageId) { this.pageId.add(pageId); }

    public void insertPageId (String pid) {this.pageId.add(pid);}

    public void deletePageId (String pid) {this.pageId.remove(pid);}


    public String getStartPage() {
        return startPage;
    }

    public void setStartPage(String startPage) {
        this.startPage = startPage;
    }

    public String getNumberofPages() {
        return numberofPages;
    }

    public void setNumberofPages(String numberofPages) {
        this.numberofPages = numberofPages;
    }

    public boolean getPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }
}

/**
 * Various adjustments to constructors were made by Peter Ly
 */
