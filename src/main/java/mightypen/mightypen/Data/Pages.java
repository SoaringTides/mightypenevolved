package mightypen.mightypen.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.ArrayList;

@Document(collection = "Pages")
public class Pages {

    @Id
    private String id;
    private ArrayList<String> choicePages;
    private String image;
    private String pageNumber;
    private String userId;
    //PARENT COMIC ID - this is the comic that contains the page
    private String comicId;

    public Pages() { }

    public Pages(String userId, String id, String pageNumber, String image, String comicId) {
        this.userId = userId;
        this.id = id;
        this.pageNumber = pageNumber;
        this.image = image;
        this.comicId = comicId;
        this.choicePages = new ArrayList<>();
    }


    public Pages(String userId, String id, String pageNumber, String image) {
        this.userId = userId;
        this.id = id;
        this.pageNumber = pageNumber;
        this.image = image;
        this.choicePages = new ArrayList<>();
    }

    public Pages(String id, String pageNumber) {
        this.id = id;
        this.pageNumber = pageNumber;
        this.choicePages = new ArrayList<>();
    }

    public Pages(String id, String pageNumber, String userId) {
        this.id = id;
        this.pageNumber = pageNumber;
        this.userId = userId;
        this.choicePages = new ArrayList<>();
    }
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Pages(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getChoicePages() {
        return choicePages;
    }

    public void setChoicePages(ArrayList<String> pages) {
        this.choicePages = pages;
    }

    public void insertPageChoice(String pageId) { this.choicePages.add(pageId); }

    public void deletePageChoice(String pageId) { this.choicePages.remove(pageId); }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public String getComicId() {
        return comicId;
    }

    public void setComicId(String comicId) {
        this.comicId = comicId;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }
}

/**
 * Various adjustments to constructors were made by Peter Ly
 */
