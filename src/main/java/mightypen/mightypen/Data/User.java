package mightypen.mightypen.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "Users")
public class User {

    @Id
    private String userID;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private ArrayList<String> seriesIds;
    private String emailAddress;
    private ArrayList<String> subscriptions;

    public User() {
        this.seriesIds = new ArrayList<>();
        this.subscriptions = new ArrayList<>();
    }

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
        this.seriesIds = new ArrayList<>();
        this.subscriptions = new ArrayList<>();
    }

    public User(String userID, String userName, String password) {
        this.userID = userID;
        this.userName = userName;
        this.password = password;
        this.seriesIds = new ArrayList<>();
        this.subscriptions = new ArrayList<>();
    }

    public User(String userID, String userName, String password, String firstName, String lastName) {
        this.userID = userID;
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.seriesIds = new ArrayList<>();
        this.subscriptions = new ArrayList<>();
    }

    public User(String userID, String userName, String password, String firstName, String lastName, String emailAddress) {
        this.userID = userID;
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.seriesIds = new ArrayList<>();
        this.subscriptions = new ArrayList<>();
    }

    public User(String userID, String userName, String password, String firstName, String lastName, String emailAddress, ArrayList<String> subscriptions) {
        this.userID = userID;
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.seriesIds = new ArrayList<>();
        this.subscriptions = subscriptions;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<String> getSeriesIds() {
        return seriesIds;
    }

    public void setSeriesIds(ArrayList<String> seriesIds) {
        this.seriesIds = seriesIds;
    }

    public void addSeriesID(String seriesId) { this.seriesIds.add(seriesId); }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public ArrayList<String> getSubscriptions() {
        if (this.subscriptions == null) { this.subscriptions = new ArrayList<>(); }
        return subscriptions;
    }

    public void addSubscriptions(String sub){
        if (this.subscriptions == null) { this.subscriptions = new ArrayList<>(); }
        this.subscriptions.add(sub);
    }

    public void removeSubscriptions(String sub) {
        if (this.subscriptions == null) { this.subscriptions = new ArrayList<>(); }
        this.subscriptions.remove(sub);
    }
}

/**
 * Various adjustments to constructors were made by Peter Ly
 */
