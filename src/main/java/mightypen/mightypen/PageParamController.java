package mightypen.mightypen;


import mightypen.mightypen.Repositories.ComicRepository;
import mightypen.mightypen.Repositories.PageRepository;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import mightypen.mightypen.Data.*;
import java.util.ArrayList;

/**
 * Customized functions pertaining to Page Management.
 * @author  Peter Ly
 * @version 1.0
 */
@RestController
@RequestMapping("/Page")
public class PageParamController {

    //@Autowired
    PageRepository pageRepo;
    ComicRepository comicRepo;
    UserRepository userRepo;

    public PageParamController(PageRepository pageRepo, ComicRepository comicRepo, UserRepository userRepo)
    {
        this.pageRepo = pageRepo;
        this.comicRepo = comicRepo;
        this.userRepo = userRepo;
    }


    //Get pages by author's userid
    @GetMapping("/pageUserId/{userId}")
    public ArrayList<Pages> getPageByUserId(@PathVariable("userId") String userId)
    {
        return pageRepo.findByUserId(userId);
    }

    @PostMapping(value=
            "/AddParamPage/{pageId}/{pagenumber}/{comicname}"
    )
    public void AddParameterizedPage(@ModelAttribute Pages page,
                                     @PathVariable("pageId") String pageId,
                                     @PathVariable("pagenumber") String pagenumber,
                                     @PathVariable("comicname") String comicname) {
        try
        {
            page.setId(pageId);
            page.setPageNumber(pagenumber);
            this.pageRepo.save(page);

            System.out.println(comicname);

            Comics comic = comicRepo.findByTitle(comicname);
            System.out.println("The comic:");
            System.out.println(comic);
            comic.insertPageId(pageId);
            //System.out.println(comic.getPageId());
            comicRepo.save(comic);

        }
        catch(Exception e)
        {
            System.out.println("Page was not added!");
            System.out.println(e);
        }
    }
    @PostMapping(value=
            "/AddParamPage/{pageId}/{pagenumber}/{comicname}/{userId}"
    )
    public void AddParameterizedPage(@ModelAttribute Pages page,
                                     @PathVariable("pageId") String pageId,
                                     @PathVariable("pagenumber") String pagenumber,
                                     @PathVariable("comicname") String comicname,
                                     @PathVariable("userId") String userId) {
        try
        {
            page.setId(pageId);
            page.setPageNumber(pagenumber);
            page.setUserId(userId);
            page.setChoicePages(new ArrayList<String>());

            System.out.println(comicname);

            Comics comic = comicRepo.findByTitle(comicname);
            System.out.println("The comic:");
            System.out.println(comic);
            comic.insertPageId(pageId);
            //System.out.println(comic.getPageId());
            comicRepo.save(comic);


            page.setComicId(comic.getComicId());
            this.pageRepo.save(page);

        }
        catch(Exception e)
        {
            System.out.println("Page was not added!");
            System.out.println(e);
        }
    }


    @PostMapping(value=
            "/UpdateParamPage/{pageId}/{pagenumber}/{comicname}/{userId}"
    )
    public void UpdateParamPage(@ModelAttribute Pages page,
                                @PathVariable("pageId") String pageId,
                                @PathVariable("pagenumber") String pagenumber,
                                @PathVariable("comicname") String comicname,
                                @PathVariable("userId") String userId) {
        try
        {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String username = "";
            if (principal instanceof UserDetails) {
                username = ((UserDetails)principal).getUsername();
            } else {
                username = principal.toString();
            }
            System.out.println(username);


            try
            {
                mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
                ArrayList<Pages> userPages = pageRepo.findByUserId(user.getUserID());
                Pages editpage = null;
                for (Pages p : userPages) {
                    if (p.getId().equals(pageId)){
                        editpage = p;
                    }
                }

                try{

                    if(!(editpage.getComicId().equals(comicname))){
                        //if you're moving a page to another comic, we need to adjust accordingly
                        //the page's id reference is deleted from old comic, moved to new
                        Comics oldcomic = comicRepo.findByTitle(editpage.getComicId());
                        Comics newcomic = comicRepo.findByTitle(comicname);
                        System.out.println("OLD COMIC");
                        System.out.println(oldcomic.getTitle());
                        System.out.println("NEW COMIC");
                        System.out.println(newcomic.getTitle());
                        oldcomic.deletePageId(pageId);
                        newcomic.insertPageId(pageId);
                    }

                    editpage.setPageNumber(pagenumber);
                    editpage.setComicId(comicname);
                    editpage.setImage(page.getImage());
                    pageRepo.save(editpage);
                }
                catch (NullPointerException e) //hack-ish way of working around pages that somehow lost their comicid.
                {
                    editpage.setPageNumber(pagenumber);
                    editpage.setComicId(comicname);
                    editpage.setImage(page.getImage());
                    pageRepo.save(editpage);
                }

            }
            catch (Exception e)
            {
                System.out.println("Pages not linked");
                System.out.println(e);
            }
        }
        catch(Exception e)
        {
            System.out.println("Page was not updated!");
            System.out.println(e);
        }
    }


    @PostMapping(value=
            "/AddChoice/{parentPageId}/{addedPageId}"
    )
    public void AddChoice(@PathVariable("parentPageId") String parentPageId,
                          @PathVariable("addedPageId") String addedPageId) {

        //I get pages by UserId -first- because there's very little guarantee
        //different users won't give their pages the same names.
        //doing this lets me ensure we're linking pages tied to the same account.
        // - Peter
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);


        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Pages> userPages = pageRepo.findByUserId(user.getUserID());
            Pages parentpage = null;
            Pages childpage = null;
            for (Pages p : userPages) {
                if (p.getId().equals(parentPageId)){
                    parentpage = p;
                }
                if (p.getId().equals(addedPageId)){
                    childpage = p;
                }

            }

            if(!(parentpage.getComicId().equals(childpage.getComicId()))){
                System.out.println("parent and child are not in same comic!");
                throw new Exception("parent and child are not in same comic!");
            }

            System.out.println("parent");
            System.out.println(parentpage);
            System.out.println("child");
            System.out.println(childpage);

            if (parentpage!= null & childpage != null){
                parentpage.insertPageChoice(childpage.getId());
                pageRepo.save(parentpage);
                System.out.println("Insert succeeded");
            }
            else{
                System.out.println("Insert failed");
            }

        }
        catch (Exception e)
        {
            System.out.println("Pages not linked");
            System.out.println(e);
        }

    }


    @PostMapping(value=
            "/RemoveChoice/{parentPageId}/{addedPageId}"
    )
    public void RemoveChoice(@PathVariable("parentPageId") String parentPageId,
                             @PathVariable("addedPageId") String addedPageId) {

        //I get pages by UserId -first- because there's very little guarantee
        //different users won't give their pages the same names.
        //doing this lets me ensure we're linking pages tied to the same account.
        // - Peter
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);


        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Pages> userPages = pageRepo.findByUserId(user.getUserID());
            Pages parentpage = null;
            Pages childpage = null;
            for (Pages p : userPages) {
                if (p.getId().equals(parentPageId)){
                    parentpage = p;
                }
                if (p.getId().equals(addedPageId)){
                    childpage = p;
                }

            }

            System.out.println("parent");
            System.out.println(parentpage);
            System.out.println("child");
            System.out.println(childpage);

            if (parentpage!= null & childpage != null){
                parentpage.deletePageChoice(childpage.getId());
                pageRepo.save(parentpage);
                System.out.println("Insert succeeded");
            }
            else{
                System.out.println("Insert failed");
            }

        }
        catch (Exception e)
        {
            System.out.println("Pages not linked");
            System.out.println(e);
        }

    }

    //needs rework to tie to account
    @PostMapping(value=
            "/DeleteParameterizedPage/{pageId}/{comicId}"
    )
    public void DelParameterizedPage(
            @PathVariable("pageId") String pageId,
            @PathVariable("comicId") String comicId) {


        Pages targetpage = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        try
        {


            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Pages> userPages = pageRepo.findByUserId(user.getUserID());
            for (Pages p : userPages) {
                if (p.getId().equals(pageId)){
                    targetpage = p;
                }
            }

            //Pages page = pageRepo.findByid(pageId);
            if(!targetpage.getComicId().equals("DELETED")){
                Comics comic = comicRepo.findByComicId(comicId);

                comic.deletePageId(pageId);
                comicRepo.save(comic);
            }
            System.out.println("made it past comic filter");
            pageRepo.deleteByIdAndUserId(targetpage.getId(),user.getUserID());
        }
        catch(Exception e)
        {
            System.out.println("Page was not deleted!");
            System.out.println(e);
        }
    }

    //Get page by title
    @GetMapping("/pageTitle/{title}")
    public Pages getPageByTitle(@PathVariable("title") String title)
    {

        Pages targetpage = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        //return pageRepo.findByid(title);



        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Pages> userPages = pageRepo.findByUserId(user.getUserID());
            for (Pages p : userPages) {
                if (p.getId().equals(title)){
                    targetpage = p;
                }
            }

        }
        catch (Exception e)
        {
            System.out.println("Was not able to get this page.");
            System.out.println(e);
        }
        return targetpage;
    }



    //Get all pages of the current user
    @GetMapping("/getAllPagesThisUserHas")
    public ArrayList<Pages> getAllPagesThisUserHas()
    {
        ArrayList<Pages> userPages=null;
        Pages targetpage = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        //return pageRepo.findByid(title);



        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            userPages = pageRepo.findByUserId(user.getUserID());

        }
        catch (Exception e)
        {
            System.out.println("Was not able to get this page.");
            System.out.println(e);
        }
        return userPages;
    }


    //Get all pages of the current user
    @GetMapping("/getAllPageNamesThisUserHas")
    public ArrayList<String> getAllPageNamesThisUserHas()
    {
        ArrayList<String> userPageNames=new ArrayList<String>();
        Pages targetpage = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        //return pageRepo.findByid(title);



        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Pages>  userPages = pageRepo.findByUserId(user.getUserID());

            for (Pages p : userPages) {
                userPageNames.add(p.getId());
            }

        }
        catch (Exception e)
        {
            System.out.println("Was not able to get this page.");
            System.out.println(e);
        }
        return userPageNames;
    }

}
