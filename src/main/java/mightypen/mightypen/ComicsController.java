package mightypen.mightypen;

import mightypen.mightypen.Repositories.ComicRepository;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import mightypen.mightypen.Data.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

@RestController
@RequestMapping("/Comics")
public class ComicsController {

    UserRepository userRepo;
    SeriesRepository seriesRepo;
    ComicRepository comicRepo;

    public ComicsController(SeriesRepository seriesRepo, UserRepository userRepo, ComicRepository comicRepo)
    {
        this.seriesRepo = seriesRepo;
        this.userRepo = userRepo;
        this.comicRepo = comicRepo;
    }

    //Get all comics in database
    @GetMapping("/all")
    public ArrayList<Comics> getAllComics()
    {
        return (ArrayList<Comics>) comicRepo.findAll();
    }

    //Get comics by tag
    @GetMapping("/tag/{tag}")
    public ArrayList<Comics> getComicByTag(@PathVariable("tag") String tag)
    {
        //Contains all series in database
        ArrayList<Comics> comics = (ArrayList<Comics>) comicRepo.findAll();

        ArrayList<Comics> tagComics = new ArrayList<Comics>();


        for(int i = 0; i < comics.size(); i++)
        {
            Comics s = comics.get(i);

            ArrayList<String> comicTags = s.getTag();

            for(int j = 0; j < comicTags.size(); j++)
            {
                if(comicTags.get(i).equals(tag))
                {
                    tagComics.add(s);
                }
            }

        }

        return tagComics;
    }

    @PutMapping(path = "/UpdateComic")
    public void update(@ModelAttribute Comics comic) {
        //String id = user.getUserID();

        this.comicRepo.insert(comic);

    }

    @PostMapping(path = "/AddComic")
    public void add(@ModelAttribute Comics comic) {

        try
        {
            this.comicRepo.save(comic);
        }
        catch(Exception e)
        {
            System.out.println("Comic was not added!");
        }

    }

    @PostMapping(path = "/deleteComic")
    public void delete(@ModelAttribute Comics comic) {

        try
        {
            this.comicRepo.deleteById(comic.getComicId());
        }
        catch (Exception e)
        {
            System.out.println("Comic was not deleted!");
        }
    }
}
