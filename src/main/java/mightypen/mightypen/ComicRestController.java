package mightypen.mightypen;

import mightypen.mightypen.Repositories.ComicRepository;
import mightypen.mightypen.Repositories.PageRepository;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import mightypen.mightypen.Data.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

/**
 * Rest controller to grab Comics by UserId
 * @author  Peter Ly
 * @version 1.0
 */
@RestController
@RequestMapping("/Comics")
public class ComicRestController {


    UserRepository userRepo;
    SeriesRepository seriesRepo;
    ComicRepository comicRepo;
    PageRepository pageRepo;

    public ComicRestController(SeriesRepository seriesRepo, UserRepository userRepo, ComicRepository comicRepo, PageRepository pageRepo)
    {
        this.seriesRepo = seriesRepo;
        this.userRepo = userRepo;
        this.comicRepo = comicRepo;
        this.pageRepo = pageRepo;
    }

    //Get comic by id
    //these two methods were ambiguous, resolved by adding distinct identifiers
    @GetMapping("/comicId/{comicId}")
    public Comics getComicById(@PathVariable("comicId") String comicId)
    {
        return comicRepo.findByComicId(comicId);
    }

    //Get comic by title
    //these two methods were ambiguous, resolved by adding distinct identifiers
    @GetMapping("/comicTitle/{title}")
    public Comics getComicByTitle(@PathVariable("title") String title)
    {
        return comicRepo.findByTitle(title);
    }

    //Get comic by author's userid
    @GetMapping("/comicUserId/{userId}")
    public ArrayList<Comics> getComicByUserId(@PathVariable("userId") String userId)
    {
        return comicRepo.findByUserId(userId);
    }


    //Delete Comic by Title AND UserId (this is to prevent same-named comics from being deleted!)
    //we use spring security to get the user's username
    //so that we can just do a database call here to get ID.
    //then we'll pull all the user's comics, determine which one matches
    //the title entered, and delete only that one through its id
    @PostMapping(path = "/deleteComicByUserIdScan/{title}")
    public void deleteComicByUserIdScan(@PathVariable("title") String title) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);

        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Comics> comicsarray = comicRepo.findByUserId(user.getUserID());
            Comics target = null;
            System.out.println("Title is:");
            System.out.println(title);
            for (Comics c : comicsarray)
            {
                System.out.println(c.getTitle());
                if (c.getTitle().equals(title)){
                    target = c;
                }
            }
            if(target != null){

                System.out.println(target.getSeriesId());
                if(!target.getSeriesId().equals("")){
                    Series s = seriesRepo.findBySeriesID(target.getSeriesId());
                    s.deleteComicId(target.getComicId());
                    seriesRepo.save(s);
                }

                System.out.println("page repo reached");
                ArrayList<Pages> pagearray = pageRepo.findByUserId(user.getUserID());
                //System.out.println("page loop reached");
                for (Pages p : pagearray)
                {
                    if (p.getComicId().equals(target.getComicId())){
                        p.setComicId("DELETED");
                        pageRepo.save(p);
                    }
                }
                comicRepo.deleteById(target.getComicId());
                //note that this only deletes the last named comic assuming there's multiples
                // with same comic and userid maker
                // (which there shouldn't be)
            }
            else{
                System.out.println("comic not found");
            }

        }
        catch (Exception e)
        {
            System.out.println("Comic  was not deleted!");
            System.out.println(e);
        }
    }

}
