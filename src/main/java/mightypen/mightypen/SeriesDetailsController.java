package mightypen.mightypen;

//import mightypen.mightypen.Data.Series;
//import mightypen.mightypen.Data.User;
import mightypen.mightypen.Data.*;
//import mightypen.mightypen.Repositories.SeriesRepository;
//import mightypen.mightypen.Repositories.UserRepository;
import mightypen.mightypen.Repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Customized functions pertaining to Series searching with regex expressions.
 * @author  Peter Ly
 * @version 1.0
 */

@RestController
@RequestMapping("/SDC")
public class SeriesDetailsController {


    //@Autowired
    SeriesRepository seriesRepo;
    PageRepository pageRepo;
    ComicRepository comicRepo;
    UserRepository userRepo;

    public SeriesDetailsController(SeriesRepository seriesRepo, PageRepository pageRepo, ComicRepository comicRepo, UserRepository userRepo)
    {
        this.seriesRepo = seriesRepo;
        this.pageRepo = pageRepo;
        this.comicRepo = comicRepo;
        this.userRepo = userRepo;
    }

    //CustomerRepository customerRepository;

    //returns a series by title

    @GetMapping("/SeriesCorrectedSearch/{title}")
    public Series seriesCorrectedSearch(@PathVariable("title") String title)
    {
        System.out.print("Exact search");

        System.out.println(seriesRepo.findByTitle(title));
        Series series = seriesRepo.findByTitle(title);
        return series;
        //System.out.println(seriesRepo.findAll());
    }

    //returns a series, using partial search

    @GetMapping("/SeriesPartialSearch/{title}")
    public List<Series> seriesPartialSearch(@PathVariable("title") String title)
    {
        System.out.print("Partial search");

        //System.out.println(seriesRepo.findByTitle(title));
        //Series series = seriesRepo.findByTitle(title);
        List<Series> seriesList = seriesRepo.findByTitleRegex(title);
        return seriesList;
        //System.out.println(seriesRepo.findAll());
    }

    @GetMapping("/SeriesPartialSearchAuthor/{author}")
    public List<Series> seriesPartialSearchAuthor(@PathVariable("author") String author)
    {
        System.out.println("Partial search");

        //System.out.println(seriesRepo.findByTitle(title));
        //Series series = seriesRepo.findByTitle(title);
        List<Series> seriesList = seriesRepo.findByAuthorRegex(author);
        return seriesList;
        //System.out.println(seriesRepo.findAll());
    }

    @GetMapping("/SeriesBlankSearch/")
    public List<Series> SeriesBlankSearch()
    {
        System.out.print("All search");

        //System.out.println(seriesRepo.findByTitle(title));
        //Series series = seriesRepo.findByTitle(title);
        List<Series> seriesList = seriesRepo.findAll();
        return seriesList;
        //System.out.println(seriesRepo.findAll());
    }



    //change this to have extra seucrity
    @PostMapping("/PublishSeries/{title}")
    public void PublishSeries(@PathVariable("title") String title)
    {
        //System.out.print("Exact search");


        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);





        Series s = null;
        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Series>  userSeries = seriesRepo.findByUserId(user.getUserID());

            for (Series stone : userSeries) {
                if (stone.getTitle().equals(title)){
                    if(stone.getUserId().equals(user.getUserID())){
                        s = stone;
                        //s.setSeriesID(stone.getSeriesID());
                    }
                }
            }

            if(s.getPublished() != true){
                s.setPublished(true);
            }
            else{
                s.setPublished(false);
            }
            seriesRepo.save(s);
        }
        catch (Exception e)
        {
            System.out.println("Was not able to get this page.");
            System.out.println(e);
        }


        /*
        System.out.println(seriesRepo.findByTitle(title));
        Series series = seriesRepo.findByTitle(title);
        if(series.getPublished() != true){
            series.setPublished(true);
        }
        else{
            series.setPublished(false);
        }
        seriesRepo.save(series);
         */
        //System.out.println(seriesRepo.findAll());
    }

}
