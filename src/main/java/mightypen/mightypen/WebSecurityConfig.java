package mightypen.mightypen;


import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Web security module permitting various files and functions
 * @author  Peter Ly
 * @version 1.0
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //placed here to demonstrate the main issue
        //http.authorizeRequests().anyRequest().permitAll();
        http.csrf().disable();

        //Uncomment this section and comment out the above to get regular security back.
        http
                .authorizeRequests()
                .antMatchers("/styles/*",
                        "/images/*",
                        "/js/*",
                        "/register.html",
                        "/byUser/*",
                        "/Series/*",
                        "/Comics/*",
                        "/Page/*", //this i'l be taking out
                        "/AddUserAndRedirect").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll();

    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Service
    public class MyUserDetailsService implements UserDetailsService {

        @Autowired
        private UserRepository userRepository;

        @Override
        public UserDetails loadUserByUsername(String username) {

            mightypen.mightypen.Data.User user = userRepository.findByUserName(username);
            //System.out.println("loadUserByUsername triggered");

            if (user == null) {
                //System.out.println("No user found with username: "+ username);
                throw new UsernameNotFoundException("No user found with username: "+ username);
            }
            //System.out.println("loadUserByUsername successful");
            Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>(1);
            grantedAuthorities.add(new SimpleGrantedAuthority("USER"));
            return new org.springframework.security.core.userdetails.User(
                    user.getUserName(),
                    passwordEncoder().encode(user.getPassword()),
                    grantedAuthorities);
        }
    }
}
