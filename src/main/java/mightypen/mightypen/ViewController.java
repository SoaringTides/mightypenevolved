package mightypen.mightypen;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController()
public class ViewController {

/*
    @GetMapping("/")
    public ModelAndView getHome()
    {
        ModelAndView mv = new ModelAndView("index.html");
        return mv;
    }
*/
    @GetMapping("/manageAccount")
    public ModelAndView manageAccount()
    {

        ModelAndView mv = new ModelAndView("manageAccount.html");
        return mv;
    }

    @GetMapping("/manageComic")
    public ModelAndView manageComic()
    {

        ModelAndView mv = new ModelAndView("manageComic.html");
        return mv;
    }

    @GetMapping("/managePage")
    public ModelAndView managePage()
    {

        ModelAndView mv = new ModelAndView("managePage.html");
        return mv;
    }

    @GetMapping("/manageSeries")
    public ModelAndView manageSeries()
    {

        ModelAndView mv = new ModelAndView("manageSeries.html");
        return mv;
    }
    @GetMapping("/register")
    public ModelAndView register()
    {

        ModelAndView mv = new ModelAndView("register.html");
        return mv;
    }

    @GetMapping("/search")
    public ModelAndView search()
    {

        ModelAndView mv = new ModelAndView("search.html");
        return mv;
    }

    @GetMapping("/viewComic")
    public ModelAndView viewComic()
    {

        ModelAndView mv = new ModelAndView("viewComic.html");
        return mv;
    }

    @GetMapping("/subscription")
    public ModelAndView subscription()
    {

        ModelAndView mv = new ModelAndView("subscription.html");
        return mv;
    }




}
