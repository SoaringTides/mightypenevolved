package mightypen.mightypen;

import mightypen.mightypen.Repositories.ComicRepository;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import mightypen.mightypen.Data.*;

import java.util.ArrayList;
@RestController
@RequestMapping("/Series")
public class SeriesController {

    UserRepository userRepo;
    SeriesRepository seriesRepo;
    ComicRepository comicRepo;

    public SeriesController(SeriesRepository seriesRepo, UserRepository userRepo, ComicRepository comicRepo)
    {
        this.seriesRepo = seriesRepo;
        this.userRepo = userRepo;
        this.comicRepo = comicRepo;
    }

    //Returns all series in database
    @GetMapping("/")
    public ArrayList<Series> getAllSeries()
    {   System.out.println("fsdfsdfsd");
        ArrayList<Series> series = (ArrayList<Series>) seriesRepo.findAll();

        return series;
    }

    //Returns a series by its id
    @GetMapping("/seriesId/{seriesId}")
    public Series getSeriesById(@PathVariable("seriesId") String seriesId)
    {
        Series series = seriesRepo.findBySeriesID(seriesId);

        return series;
    }
    //returns a series by title
    @GetMapping("/title/{seriesTitle}")
    public Series getSeriesByTitle(@PathVariable("seriesTitle") String seriesTitle)
    {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails)principal).getUsername();
        } else {
            username = principal.toString();
        }
        System.out.println(username);


        Series s = null;
        try
        {
            mightypen.mightypen.Data.User user = userRepo.findByUserName(username);
            ArrayList<Series>  userSeries = seriesRepo.findByUserId(user.getUserID());

            for (Series stone : userSeries) {
                if (stone.getTitle().equals(seriesTitle)){
                    if(stone.getUserId().equals(user.getUserID())){
                        s = stone;
                        //s.setSeriesID(stone.getSeriesID());
                    }
                }
            }

            if(s.getPublished() != true){
                s.setPublished(true);
            }
            else{
                s.setPublished(false);
            }
            seriesRepo.save(s);
        }
        catch (Exception e)
        {
            System.out.println("Was not able to get this page.");
            System.out.println(e);
        }

        return s;
        /*
        Series series = seriesRepo.findByTitle(seriesTitle);

        return series;
         */
    }


    //Returns all series of a type
    @GetMapping("/tag/{tag}")
    public ArrayList<Series> getSeriesByTag(@PathVariable("tag") String tag)
    {
        System.out.println(tag);
        //Contains all series in database
        ArrayList<Series> series = (ArrayList<Series>) seriesRepo.findAll();

        ArrayList<Series> tagSeries = new ArrayList<Series>();
        for(int i = 0; i < series.size(); i++)  {
            Series s = series.get(i);
            ArrayList<String> seriesTags = s.getTag();
            if (seriesTags != null) {
                for (int j = 0; j < seriesTags.size(); j++) {
                    if(seriesTags.get(j).equals(tag)){
                        tagSeries.add(s);
                        break;
                    }
                }
            }
        }
        return tagSeries;
    }

    //returns all comics in a series
    @GetMapping("/series/{seriesId}/comics")
    public ArrayList<Comics> getSeriesComics(@RequestParam("title") String seriesId)
    {
        Series series = seriesRepo.findByTitle(seriesId);

        ArrayList<String> comicIds = series.getComicID();

        ArrayList<Comics> comics = new ArrayList<Comics>();

        for(int i = 0; i < comicIds.size(); i++)
        {
            comics.add(comicRepo.findByComicId(comicIds.get(i)));
        }

        return comics;
    }


    @PostMapping(path = "/UpdateSeriesTags/{seriesId}/{tags}")
    public void updateTags(@PathVariable String seriesId,
                           @PathVariable ArrayList<String> tags) {
        System.out.println(seriesId);
        System.out.println(tags);
        try {
            Series series = this.seriesRepo.findBySeriesID(seriesId);
            if (tags.get(0).equals("eMpTy")) {
                series.setTag(new ArrayList<>());
            } else {
                series.setTag(tags);
            }
            this.seriesRepo.save(series);
        } catch (Exception e) {
            System.out.println("Could not update series tags!");
        }
    }

    @PostMapping(path = "/AddSeries")
    public void add(@ModelAttribute Series series) {

        try
        {
            this.seriesRepo.save(series);
            System.out.println("Series added");
        }
        catch(Exception e)
        {
            System.out.println("User was not added!");
        }

    }

    @PostMapping(path = "/deleteSeries")
    public void delete(@ModelAttribute Series series) {

        try
        {
            this.seriesRepo.deleteById(series.getSeriesID());
        }
        catch (Exception e)
        {
            System.out.println("User was not deleted!");
        }
    }
}
