package mightypen.mightypen;


import mightypen.mightypen.Repositories.ComicRepository;
import mightypen.mightypen.Repositories.PageRepository;
import mightypen.mightypen.Repositories.SeriesRepository;
import mightypen.mightypen.Repositories.UserRepository;
import org.springframework.web.bind.annotation.*;
import mightypen.mightypen.Data.*;
import java.util.ArrayList;

@RestController
@RequestMapping("/Page")
public class PageController {

    UserRepository userRepo;
    SeriesRepository seriesRepo;
    ComicRepository comicRepo;
    PageRepository pageRepo;

    public PageController(SeriesRepository seriesRepo, UserRepository userRepo,PageRepository pageRepo)
    {
        this.seriesRepo = seriesRepo;
        this.userRepo = userRepo;
        this.comicRepo = comicRepo;
        this.pageRepo = pageRepo;
    }

    @GetMapping("/{pageId}")
    public Pages getPageById(@PathVariable("pageId") String pageId)
    {
        return pageRepo.findByid(pageId);
    }

    @GetMapping("/{pageId}/pages")
    public ArrayList<Pages> getPages(@PathVariable("pageId") String pageId)
    {
        ArrayList<Pages> pages = new ArrayList<Pages>();
        Pages page = pageRepo.findByid(pageId);

        ArrayList<String> pageIds = page.getChoicePages();

        for(int i = 0; i < pageIds.size(); i++)
        {
            pages.add(pageRepo.findByid(pageIds.get(i)));
        }

        return pages;
    }

    @PutMapping(path = "/UpdatePage")
    public void update(@ModelAttribute Pages page) {


        this.pageRepo.insert(page);

    }

    @PostMapping(path = "/AddPage")
    public void add(@ModelAttribute Pages page) {

        try
        {
            this.pageRepo.save(page);
        }
        catch(Exception e)
        {
            System.out.println("Page was not added!");
        }

    }

    @PostMapping(path = "/deletePage")
    public void delete(@ModelAttribute Pages page) {

        try
        {
            this.pageRepo.deleteById(page.getId());
        }
        catch (Exception e)
        {
            System.out.println("Page was not deleted!");
        }
    }
}
